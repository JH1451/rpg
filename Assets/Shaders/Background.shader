﻿Shader "Custom/Background"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Colour("Background Colour", color) = (0,0,0)
		_MovementSpeed("MovementSpeed", float) = 4
	}
	SubShader
	{
		Tags { "Queue" = "Transparent" }
		LOD 100

		Pass
		{

			Cull Back
			ZWrite On
			Ztest Greater

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _Colour;
			float _MovementSpeed;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv + float2(
					((_Time[1] * _MovementSpeed) / 3),
					(sin(i.vertex.x / 50 + (_Time[1] * _MovementSpeed)) / 3)));

				col = col / 2;

				col += tex2D(_MainTex, i.uv + float2(
					((-_Time[1] * _MovementSpeed) / 3),
					(-sin(i.vertex.x / 50 + (_Time[1] * _MovementSpeed)) / 3))) / 2;

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);


				col *= _Colour;

				return col;
			}
			ENDCG
		}
	}
}
