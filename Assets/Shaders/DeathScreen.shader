﻿Shader "Custom/DeathScreen"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Multiplier("Posturise multiplier", int) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Multiplier;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			float GetRoundedFloat(float input)
			{
				input *= 10 * _Multiplier;
				input = round(input);
				return input / 10 * _Multiplier;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);

				//apply screen wobble
				fixed4 col = tex2D(_MainTex, i.uv + float2(
					sin(i.vertex.y / 75 + (_Time[1] * 0.5)) / 120,
					sin(i.vertex.x / 75 + (_Time[1] * 0.5)) / 120));

				//posturise
				col = fixed4(
					GetRoundedFloat(col.r),
					GetRoundedFloat(col.g),
					GetRoundedFloat(col.b),
					col.a);

				//redden
				col.r = 1;

				return col;
			}
			ENDCG
		}
	}
}
