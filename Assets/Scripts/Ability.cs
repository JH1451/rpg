﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ability //stores information for a specific ability. not monobehaviour so it can be instantiated as a type without a gameobject
{ 

    public enum Type { nullType = 0, projectile = 1, explosion = 2, missile = 3, shockwave = 4, laser = 5, armorBuff = -1, energyBuff = -2, healthBuff = -3};
    

    public Type abilityType = Type.nullType;

    public string abilityName = "";
    public int damage = 20, energyCost = 20;
    public bool active = false, isSuper = false;
    public float speed = 0.01f,
                 maxScaleMagnitude = 5f,
                 timeout = 10f,
                 cooldown = 2f,
                 radius = 1f,
                 passiveValue = 0f;
    public static float overclockMultiplier = 1;

    public bool IsPassive { get { return ((int)abilityType < 0); } }
    public float CooldownDecimal { get { return (cooldown - cooldownTimer) / cooldown; } } //returns percentage of the cooldown left in a decimal
    public int Damage { get { return Mathf.RoundToInt((float)damage * overclockMultiplier); } }

    private float movementTimer = 0, timeoutTimer = 0, cooldownTimer = 0;
    
    public string Name { get { return AbilityController.GetNameFromAbilityType(abilityType); } }
    public string Desc { get { return AbilityController.GetDescFromAbilityType(abilityType); } }


    public Ability()
    {
        //
    }

    public Ability(Type typeInput, float valueInput) //for passive ability
    {
        abilityType = typeInput;
        passiveValue = valueInput;
    }

    public Ability(Type typeInput, int damageInput, int energyCostInput, float cooldownInput) //for active ability
    {
        damage = damageInput;
        abilityType = typeInput;
        damage = damageInput;
        energyCost = energyCostInput;
        cooldown = cooldownInput;
    }

    public bool Activate() //called when ability is used, deals with cooldown and energy cost, returns true if ability can be used
    {
        if (!active)
        {
            if (PlayerController.player.UseEnergy(energyCost))
            {
                active = true;
                return true;
            }
            return false;
        }
        return false;
    }

    public void UpdateTimer(float deltaTime) //called every frame from AbilityController, used in place of monobehaviour update()
    {
        if (active)
        {
            cooldownTimer += deltaTime;
            if (cooldownTimer >= cooldown)
            {
                active = false;
                cooldownTimer = 0;
            }
        }
    }

    public bool EnemiesCanUse //returns true if an enemy can use this ability
    {
        get
        {
            switch (abilityType)
            {
                case Type.projectile:
                    return true;
                case Type.missile:
                    return true;
                default:
                    return false;
            }
        }
    }
}
