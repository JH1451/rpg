﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureController : MonoBehaviour {

    public GameObject creaturePrefabInspector;
    public static GameObject creaturePrefab;

    [System.Serializable]
    public class CreatureInput
    {
        public EnemyController.EnemyType type;
        public Sprite avatar;
        public GameObject creatureObject;
    }

    [SerializeField]
    public List<CreatureInput> creatureVisualsInput = new List<CreatureInput>();
    public static List<CreatureInput> creatureVisuals = new List<CreatureInput>();
    

	// Use this for initialization
	void Start () {
        creaturePrefab = creaturePrefabInspector;
        creatureVisuals = creatureVisualsInput;
        
    }

    public static GameObject SpawnCreature(int slot, Creature creature) //spawnes and configures a creature
    {
        Debug.Log("Spawn creature called");

        GameObject newCreature = Instantiate(GetCreatureObjectFromType(creature.type), PlayerController.playerObject.transform.position, Quaternion.identity);
        newCreature.GetComponent<CreatureBehaviour>().creature = creature;
        newCreature.GetComponent<CreatureBehaviour>().creature.behaviour = newCreature.GetComponent<CreatureBehaviour>();
        newCreature.GetComponent<CreatureBehaviour>().creature.isEquipped = true;
        newCreature.GetComponent<CreatureBehaviour>().currentSlot = slot;

        return newCreature;
    }

    public static GameObject GetCreatureObjectFromType(EnemyController.EnemyType inputType) //returns a creature gameobject of the correct type
    {
        List<GameObject> typeMaches = new List<GameObject>();

        for (int i = 0; i < creatureVisuals.Count; i++) //add all matched types to a list
        {
            if (creatureVisuals[i].type == inputType)
                typeMaches.Add(creatureVisuals[i].creatureObject);
        }

        return typeMaches[Random.Range(0, typeMaches.Count)]; //return randomly from that list
    }

    public static Sprite GetAvatarFromType(EnemyController.EnemyType inputType)
    {
        Sprite returnSprite = creatureVisuals[0].avatar;

        for (int i = 0; i < creatureVisuals.Count; i++) //add all matched types to a list
        {
            if (creatureVisuals[i].type == inputType)
                return creatureVisuals[i].avatar;
        }

        return returnSprite;
    }
}
