﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityController : MonoBehaviour { //manages ability controls and creation


    public LayerMask enemyLayerInspector, groundLayerInspector, playerLayerInspector;
    public static LayerMask enemyLayer, groundLayer, playerLayer;
    public GameObject projectileObjectInspector;
    public GameObject explosionObjectInspector;
    public GameObject missileObjectInspector;
    public GameObject shockwaveObjectInspector;
    public GameObject laserObjectInspector;
    public static GameObject shockwaveObject;
    public static GameObject projectileObject;
    public static GameObject missileObject;
    public static GameObject explosionObject;
    public static GameObject laserObject;

    [SerializeField]
    private static float projectileSpawnDistanceMultiplier = 1f;

    public class AbilityEquipSlot //container used to manage each of the 4 ability slots
    {
        public static int selectedAbility = 0;
        public static int SelectedAbility
        {
            get { return selectedAbility; }
            set
            {
                if (equippedAbilities[value].IsEquipped)
                    selectedAbility = value;
            }
        }
        private bool isEquipped = false;
        public bool IsEquipped { get { return (creature.active.abilityType != Ability.Type.nullType); } }
        public Creature creature = new Creature();
        public void Remove()
        { creature.isEquipped = false; }
    }

    [System.Serializable]
    public class AbilityVisuals //container used for inputing the various spries used for each ability though the inspector
    {
        public Ability.Type type;
        public Sprite sprite;
    }
    public List<AbilityVisuals> abilityVisualsInput = new List<AbilityVisuals>();
    public static List<AbilityVisuals> abilityVisualsStatic = new List<AbilityVisuals>();

    public static AbilityEquipSlot[] equippedAbilities = new AbilityEquipSlot[4]; //list of all equipped abilities
    public static AbilityEquipSlot[] equippedAbilitiesSave = new AbilityEquipSlot[4]; //list of all equipped abilities saved from last hub visit
    
    public static List<Ability> playerAbilities = new List<Ability>();
    

	void Start () {
        projectileObject = projectileObjectInspector; //loads all of the inputs from the inspector into their static versions for easy acces
        explosionObject = explosionObjectInspector;
        missileObject = missileObjectInspector;
        shockwaveObject = shockwaveObjectInspector;
        laserObject = laserObjectInspector;
        enemyLayer = enemyLayerInspector;
        groundLayer = groundLayerInspector;
        playerLayer = playerLayerInspector;

        abilityVisualsStatic = abilityVisualsInput;
        
        for (int i = 0; i < equippedAbilities.Length; i++) //initialises the ability slots
        {
            if (equippedAbilities[i] == null)
            {
                equippedAbilities[i] = new AbilityEquipSlot();
                equippedAbilitiesSave[i] = new AbilityEquipSlot();
            }
        }
        
        EquipAll();
    }
	

	void Update () {

        if (!PlayerController.player.IsDead) //if player is alive
        {
            for (int i = 0; i < equippedAbilities.Length; i++)
            {
                if (equippedAbilities[i].IsEquipped)
                    equippedAbilities[i].creature.active.UpdateTimer(Time.deltaTime); //update cooldown timers on actor class
            }


            if (Input.GetAxis("Ability 1") != 0) //bind abilities to controls
            {
                if (equippedAbilities[0].IsEquipped)
                    UseAbility(0);
            }
            if (Input.GetAxis("Ability 2") != 0)
            {
                if (equippedAbilities[1].IsEquipped)
                    UseAbility(1);
            }
            if (Input.GetAxis("Ability 3") != 0)
            {
                if (equippedAbilities[2].IsEquipped)
                    UseAbility(2);
            }
            if (Input.GetAxis("Ability 4") != 0)
            {
                if (equippedAbilities[3].IsEquipped)
                    UseAbility(3);
            }


            if (Input.GetAxis("Select1") != 0) //bind controlls for setting main ability
                AbilityEquipSlot.SelectedAbility = 0;

            if (Input.GetAxis("Select2") != 0)
                AbilityEquipSlot.SelectedAbility = 1;

            if (Input.GetAxis("Select3") != 0)
                AbilityEquipSlot.SelectedAbility = 2;

            if (Input.GetAxis("Select4") != 0)
                AbilityEquipSlot.SelectedAbility = 3;



            if (Input.GetAxis("Fire2") != 0) //binds main ability
                UseAbility(AbilityEquipSlot.selectedAbility);

            ApplyPassiveBuffs();
        }
    }

    public void UseAbility(int slot) //activates an ability
    {
        if (equippedAbilities[slot].IsEquipped)
        {
            if (equippedAbilities[slot].creature.active.Activate()) //if ability can be used (enough energy and not in cooldown)
            {
                Vector3 casterPosition = equippedAbilities[slot].creature.behaviour.transform.position;
                casterPosition.y = PlayerController.playerObject.transform.position.y;
                switch (equippedAbilities[slot].creature.active.abilityType) //spawn a specific ability
                {
                    case Ability.Type.projectile:
                        CastProjectile(casterPosition, GetCreatureToMouseRotation(casterPosition), equippedAbilities[slot].creature.active.Damage, 20, true);
                        break;
                    case Ability.Type.explosion:
                        CastExplosion(GetCollisionPoint(GetCameraToMouseDirection()), equippedAbilities[slot].creature.active.Damage, 15);
                        break;
                    case Ability.Type.missile:
                        CastMissile(casterPosition, GetCreatureToMouseRotation(casterPosition), equippedAbilities[slot].creature.active.Damage, 10, true);
                        break;
                    case Ability.Type.shockwave:
                        CastShockwave(casterPosition, GetCreatureToMouseRotation(casterPosition), equippedAbilities[slot].creature.active.Damage, 1f);
                        break;
                    case Ability.Type.laser:
                        CastLaser(casterPosition, GetCreatureToMouseDirection(casterPosition), GetCreatureToMouseRotation(casterPosition),
                            equippedAbilities[slot].creature.active.Damage);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public static void UseEnemyAbility(Ability.Type abilityType, Vector3 castPosition, Quaternion direction, int damage) //UseAbility() for enemies
    {
        switch (abilityType)
        {
            case Ability.Type.projectile:
                CastProjectile(castPosition, direction, damage, 16, false);
                break;
            case Ability.Type.missile:
                CastMissile(castPosition, direction, damage, 16, false);
                break;
        }
    }

    public static void CastProjectile(Vector3 position, Quaternion direction, int damage, float speed, bool friendly) //instantiates a projectile prefab with specific properties
    {
        Quaternion projectileRotation = Quaternion.Euler(direction.x, 0f, direction.z);
        Vector3 castPosition = position + (direction * Vector3.forward * projectileSpawnDistanceMultiplier);
        

        GameObject projectile = Instantiate(projectileObject, castPosition, direction);
        projectile.GetComponent<AbilityBehaviour>().speed = speed;
        projectile.GetComponent<AbilityBehaviour>().abilityType = Ability.Type.projectile;
        projectile.GetComponent<AbilityBehaviour>().damage = damage;
        projectile.GetComponent<AbilityBehaviour>().friendly = friendly;
    }

    public static void CastExplosion(Vector3 position, int damage, float speed) //instantiates an explosion prefab with specific properties
    {
        if (position != (new Vector3(0, 0, 0)))
        {
            GameObject explosion = Instantiate(explosionObject, position, Quaternion.identity);
            explosion.GetComponent<AbilityBehaviour>().abilityType = Ability.Type.explosion;
            explosion.GetComponent<AbilityBehaviour>().speed = speed;
            explosion.GetComponent<AbilityBehaviour>().damage = damage;
        }
    }

    public static void CastMissile(Vector3 position, Quaternion direction, int damage, float speed, bool friendly) //instantiates a projectile prefab that creates an explosion
    {
        Quaternion projectileRotation = Quaternion.Euler(direction.x, 0f, direction.z);
        Vector3 castPosition = position + (direction * Vector3.forward * projectileSpawnDistanceMultiplier);
        GameObject projectile = Instantiate(missileObject, castPosition, direction);
        projectile.GetComponent<AbilityBehaviour>().speed = speed;
        projectile.GetComponent<AbilityBehaviour>().abilityType = Ability.Type.missile;
        projectile.GetComponent<AbilityBehaviour>().damage = damage;
        projectile.GetComponent<AbilityBehaviour>().friendly = friendly;
    }

    public static void CastShockwave(Vector3 position, Quaternion direction, int damage, float speed) //instantiates a shockwave prefab with specific properties
    {
        Quaternion projectileRotation = Quaternion.Euler(direction.x, 0f, direction.z);
        Vector3 castPosition = position + (direction * Vector3.forward * projectileSpawnDistanceMultiplier * 2.5f);


        GameObject shockwave = Instantiate(shockwaveObject, castPosition, direction);
        shockwave.GetComponent<AbilityBehaviour>().speed = speed;
        shockwave.GetComponent<AbilityBehaviour>().abilityType = Ability.Type.shockwave;
        shockwave.GetComponent<AbilityBehaviour>().damage = damage;
    }

    public static void CastLaser(Vector3 position, Vector3 direction, Quaternion rotation, int damage)
    {
        Quaternion laserRotation = Quaternion.Euler(90f, 0f, rotation.z);
        Vector3 castPosition = position;

        float playerPosY = PlayerController.playerObject.transform.position.y + 2f;

        GameObject hitObject;
        Vector3 hitPosition = Vector3.zero;
        int hitResult = RayCastHitEnemy(castPosition, direction, out hitObject, out hitPosition);
        
        if (hitResult == 2) //if raycast hit nothing
        {
            //
        }
        else
        {
            castPosition = new Vector3(castPosition.x, playerPosY, castPosition.z);
            hitPosition = new Vector3(hitPosition.x, playerPosY, hitPosition.z);
            
            Vector3 midPoint = (castPosition + hitPosition) / 2;
            float length = Vector3.Distance(castPosition, hitPosition);

            GameObject laser = Instantiate(laserObject, midPoint, rotation);
            laser.transform.GetChild(0).localScale = new Vector3(laser.transform.localScale.x, length, laser.transform.localScale.z);

            if (hitResult == 0) //if raycast hits enemy
            {
                if (hitObject.GetComponent<Actor>() != null)
                    hitObject.GetComponent<Actor>().Damage(damage);
            }
            else
                Destroy(hitObject);
        }
    }

    public static void CastBossAttack(Vector3 position, Quaternion direction, int damage, float speed, bool friendly)
    {
        Quaternion projectileRotation = Quaternion.Euler(direction.x, 0f, direction.z);
        Vector3 castPosition = position + (direction * Vector3.forward * projectileSpawnDistanceMultiplier);
        GameObject projectile = Instantiate(projectileObject, castPosition, direction);
        projectile.GetComponent<AbilityBehaviour>().speed = speed;
        projectile.GetComponent<AbilityBehaviour>().abilityType = Ability.Type.missile;
        projectile.GetComponent<AbilityBehaviour>().damage = damage;
        projectile.GetComponent<AbilityBehaviour>().friendly = friendly;
    }

    private Vector3 GetCollisionPoint(Vector3 direction) //returns the point where a ray from the camera in a specific direction collides
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit collision;
        Physics.Raycast(ray, out collision, 400f);

        return collision.point;
    }

    private Vector3 GetCameraToMouseDirection() //gets the direction of the mouse from the camera
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 10;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        Vector3 direction = Vector3.Normalize(mousePos - CameraController.position);

        return direction;
    }

    private Quaternion GetPlayerToMouseRotation() //gets the rotation from the player to the mouse when projected on a plane
    {
        Quaternion returnValue = Quaternion.identity;
        Vector3 mousePos = GetIntersectionWithPlane(Vector3.up); //get the position of the mouse when projected on the plane y = 0
        Vector3 direction = mousePos - new Vector3(PlayerController.playerObject.transform.position.x, 0, PlayerController.playerObject.transform.position.z); ; //get the direction between mouse and player
        float angle = Vector3.Angle(Vector3.forward, direction); //find the angle that the object needs to be rotated for that direction
        if (direction.x < 0) //if the direction is pointing to the left, reverse the direction
            angle = -angle;
        returnValue.eulerAngles = new Vector3(0, angle, 0); //create a rotation at the specified angle on the y = 0 plane
        return returnValue;
    }

    private Quaternion GetCreatureToMouseRotation(Vector3 creaturePos) //gets the rotation from the player to the mouse when projected on a plane
    {
        Quaternion returnValue = Quaternion.identity;
        Vector3 mousePos = GetIntersectionWithPlane(Vector3.up); //get the position of the mouse when projected on the plane y = 0
        Vector3 direction = mousePos - new Vector3(creaturePos.x, 0, creaturePos.z); ; //get the direction between mouse and player
        float angle = Vector3.Angle(Vector3.forward, direction); //find the angle that the object needs to be rotated for that direction
        if (direction.x < 0) //if the direction is pointing to the left, reverse the direction
            angle = -angle;
        returnValue.eulerAngles = new Vector3(0, angle, 0); //create a rotation at the specified angle on the y = 0 plane
        return returnValue;
    }

    private Vector3 GetCreatureToMouseDirection(Vector3 creaturePos) //gets the rotation from the player to the mouse when projected on a plane
    {
        Quaternion returnValue = Quaternion.identity;
        Vector3 mousePos = GetIntersectionWithPlane(Vector3.up); //get the position of the mouse when projected on the plane y = 0
        Vector3 direction = mousePos - new Vector3(creaturePos.x, 0, creaturePos.z); ; //get the direction between mouse and player
        return direction;
    }

    private Vector3 GetIntersectionWithPlane(Vector3 planeInput) //gets the point where a ray from the camera to the mouse meets a specific plane
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //get ray from the camera through mouse
        Plane plane = new Plane(planeInput, 0f); //instantiate a new plane from the parameter
        float enter = 0;
        if (plane.Raycast(ray, out enter)) //if the ray collides with the plane, return the point along that ray that the collision happens
            return ray.GetPoint(enter);
        else return Vector3.zero;
    }



    public static void EquipAbility(int slot, Creature creature) //calls for specific creature to be spawned if it is equipped
    {
        for (int i = 0; i < 4; i++)
        {
            if (equippedAbilities[i].creature == creature) //unequip this ability if it is already equipped
                UnequipAbility(i);
        }

        if (!creature.isEquipped)
        {
            CreatureController.SpawnCreature(slot, creature);
        }
        equippedAbilities[slot].creature = creature;
        //ApplyPassiveBuffs();
    }

    public static void EquipAll() //calls for all equipped creatures to be spawned
    {
        for (int i = 0; i < equippedAbilities.Length; i++)
        {
            if (equippedAbilities[i].IsEquipped)
            {
                equippedAbilities[i].creature.isEquipped = false;
                EquipAbility(i, equippedAbilities[i].creature);
            }
        }
    }

    private static void UnequipAbility(int index) //unequips an ability via its index
    {
        equippedAbilities[index] = new AbilityEquipSlot();
    }

    public static void UnequipAbility(Creature creature) //unequips an ability via its creature
    {
        for (int i = 0; i < equippedAbilities.Length; i++)
        {
            if (equippedAbilities[i].creature == creature)
                equippedAbilities[i] = new AbilityEquipSlot();
        }
        
    }

    public static void ApplyPassiveBuffs() //adds all buffs together and applies them to player
    {
        int totalArmor = 0, totalEnergyBuff = 0, totalHealthBuff = 0;
        for (int i = 0; i < equippedAbilities.Length; i++)
        {
            if (equippedAbilities[i].IsEquipped)
            {
                switch (equippedAbilities[i].creature.passive.abilityType)
                {
                    case Ability.Type.armorBuff:
                        totalArmor += (int)equippedAbilities[i].creature.passive.passiveValue;
                        break;
                    case Ability.Type.energyBuff:
                        totalEnergyBuff += (int)equippedAbilities[i].creature.passive.passiveValue;
                        break;
                    case Ability.Type.healthBuff:
                        totalHealthBuff += (int)equippedAbilities[i].creature.passive.passiveValue;
                        break;
                    default:
                        break;
                }
            }
        }

        PlayerController.player.Armor = totalArmor;
        PlayerController.player.EnergyRecharge = totalEnergyBuff;
        PlayerController.player.HealthRecharge = totalHealthBuff;
    }

    static int RayCastHitEnemy(Vector3 castPosition, Vector3 direction, out GameObject hitObject, out Vector3 hitPosition) //returns 0 if enemy, 1 if terrain, 2 if nothing
    {
        hitObject = new GameObject();
        hitPosition = Vector3.zero;

        RaycastHit raycastHit;

        if (Physics.Raycast(castPosition, direction.normalized, out raycastHit, 2000f, enemyLayer))
        {
            hitObject = raycastHit.transform.gameObject;
            hitPosition = raycastHit.point;
            return 0;
        }
        if (Physics.Raycast(castPosition, direction.normalized, out raycastHit, 2000f, groundLayer))
        {
            hitPosition = raycastHit.point;
            Destroy(hitObject);
            return 1;
        }

        Destroy(hitObject);
        return 2;
    }

    public static Sprite GetSpriteFromAbilityType(Ability.Type inputType) //returns correct sprite for ability type
    {
        Sprite returnSprite = abilityVisualsStatic[0].sprite;

        for (int i = 0; i < abilityVisualsStatic.Count; i++) //add all matched types to a list
        {
            if (abilityVisualsStatic[i].type == inputType)
                return abilityVisualsStatic[i].sprite;
        }

        return returnSprite;
    }

    public static string GetNameFromAbilityType(Ability.Type type) //returns correct name for ability type
    {
        switch (type)
        {
            case Ability.Type.projectile:
                return "Projectile";
            case Ability.Type.explosion:
                return "Explosion";
            case Ability.Type.missile:
                return "Missile";
            case Ability.Type.shockwave:
                return "Shockwave";
            case Ability.Type.laser:
                return "Laser";
            case Ability.Type.armorBuff:
                return "Armor Upgrade";
            case Ability.Type.energyBuff:
                return "Recharge Module";
            case Ability.Type.healthBuff:
                return "Repair Nanobots";
            default:
                return "Error!";
        }
    }

    public static string GetDescFromAbilityType(Ability.Type type) //returns correct description for ability type
    {
        switch (type)
        {
            case Ability.Type.projectile:
                return "Fires a ball of incandescent plasma.";
            case Ability.Type.explosion:
                return "Detonates an explosive charge.";
            case Ability.Type.missile:
                return "Fires a missile with highly explosive warhead.";
            case Ability.Type.shockwave:
                return "A powerful EMP that stuns and damages enemies.";
            case Ability.Type.laser:
                return "An industrial welding laser.";
            case Ability.Type.armorBuff:
                return "Additional armour plating.";
            case Ability.Type.energyBuff:
                return "A small reactor that generates an electric charge.";
            case Ability.Type.healthBuff:
                return "Microscopic nanobots that repair damaged circuitry.";
            default:
                return "Error!";
        }
    }
}

