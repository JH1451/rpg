﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour { //prints text to the screen for pickups and other messages

    public enum TextType { damage, energy }

    public GameObject textObjectInput;
    public static GameObject textObject;

    public Color damageColourInput, energyColourInput;
    public static Color damageColour, energyColour;

    private static GameObject canvasObject;

	// Use this for initialization
	void Start () {
        canvasObject = this.gameObject;
        textObject = textObjectInput;
        damageColour = damageColourInput;
        energyColour = energyColourInput;
	}
	
	// Update is called once per frame
	void Update () {
    }

    public static void CreateText(Vector3 worldPosition, TextType type, string text) //prints text to a specivied position, type indicates colour
    {
        GameObject instantiatedObject = Instantiate(textObject, Camera.main.WorldToScreenPoint(worldPosition), Quaternion.identity, canvasObject.transform);
        instantiatedObject.GetComponent<UnityEngine.UI.Text>().text = text;

        switch (type)
        {
            case TextType.damage:
                instantiatedObject.GetComponent<UnityEngine.UI.Text>().color = damageColour;
                break;
            case TextType.energy:
                instantiatedObject.GetComponent<UnityEngine.UI.Text>().color = energyColour;
                break;
            default:
                instantiatedObject.GetComponent<UnityEngine.UI.Text>().color = Color.gray;
                break;
        }
    }
}
