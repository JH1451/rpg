﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuxiliaryCamera : MonoBehaviour {

    public enum Function { minimap, title }
    public Function cameraFunction = Function.minimap;

    private void LateUpdate()
    {
        switch (cameraFunction)
        {
            case Function.minimap: //positions minimap camera above player, does not rotate with player
                transform.position = new Vector3(
                    PlayerController.playerObject.transform.position.x,
                    PlayerController.playerObject.transform.position.y + 10,
                    PlayerController.playerObject.transform.position.z);
                transform.localRotation = Quaternion.Euler(90, 0, 0);
                break;
            case Function.title: //slowly pans title camera
                transform.rotation = Quaternion.Euler(new Vector3(0, 200 + (7 * Mathf.Sin(Time.time * 0.17f)), 0));
                break;
        }
    }
}
