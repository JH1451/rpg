﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimator : MonoBehaviour {

    [SerializeField]
    AnimationClip attackClip;
    [SerializeField]
    AnimationClip[] attackAnimations;
    [SerializeField]
    AnimationClip deathClip;
    [SerializeField]
    AnimationClip[] deathAnimations;
    [SerializeField]
    GameObject swordTrail;

    const float locoSmoothTime = 0.1f;
    bool isAlive;
    bool wait;
    float idleTime;
    float timer;
    float speedPercent;

    NavMeshAgent agent;
    Animator animator;
    AnimatorOverrideController overrideController;
    Actor actor;
    public AudioSource swordClip;

	// Use this for initialization
	void Start () {

        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        actor = GetComponent<Actor>();

        overrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = overrideController;

        isAlive = true;
        wait = true;
        timer = 0;
        idleTime = Random.Range(3, 9);
	}
	
	// Update is called once per frame
	void Update () {
        speedPercent = agent.velocity.magnitude / agent.speed;
        animator.SetFloat("speedPercent", speedPercent, locoSmoothTime, Time.deltaTime);

        if (actor.IsDead)
            OnDeath();

        timer = idleTime;
        timer -= Time.deltaTime;
        if (timer < 0)
            OnIdle();

        if (Input.GetKeyDown(KeyCode.B)) //testing
            OnAttack();
    }

    public void OnAttack()
    {
        if (!actor.IsDead && wait == true)
        {
            if (agent.isOnNavMesh)
                agent.isStopped = true;
            animator.SetTrigger("attack");
            overrideController[attackClip.name] = attackAnimations[Random.Range(0, attackAnimations.Length)];
            swordTrail.SetActive(true);
            wait = false;
            if (tag == "Player")
            {
                swordClip.volume = Random.Range(0.1f, 0.2f);
                swordClip.pitch = Random.Range(0.4f, 0.7f);
                swordClip.Play();
            }
            StartCoroutine(Wait());
        }
    }

    public void OnDeath()
    {
        if (isAlive == true)
        {
            isAlive = false;
            if (tag == "FloatBot")
                animator.Play("Death1");
            //else
            //animator.SetTrigger("death");
            //overrideController[deathClip.name] = attackAnimations[Random.Range(0, attackAnimations.Length)];
            if (tag == "Player")
            {
                animator.Play("Death");
                agent.enabled = false;
            }
            
        }
    }

    public void OnDamage()
    {
        if (agent.isOnNavMesh)
            agent.isStopped = true;
        animator.SetTrigger("damage");
        StartCoroutine(Wait());
    }

    public void OnIdle()
    {
            animator.SetTrigger("idle");
            timer = idleTime;

    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.6f);
        swordTrail.SetActive(false);
        wait = true;
        if (!actor.IsDead && agent.isOnNavMesh)
            agent.isStopped = false;
        

    }

    
}
