﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityBehaviour : MonoBehaviour //deals with ability behaviour after it has been spawned
{
    [SerializeField]
    public Ability.Type abilityType = Ability.Type.nullType;
    
    public int damage = 20;
    public float speed = 0.01f,
                 stunAmount = 3f,
                 maxScaleMagnitude = 5f,
                 timeout = 10f,
                 radius = 1f;

    private bool dealtDamage = false;
    public bool friendly = true;

    private LayerMask targetLayer;
    private float movementTimer = 0, timeoutTimer = 0;

    void Update()
    {

        if (timeoutTimer >= timeout) //destroys the gameobject if it exists for too long
            Destroy(this.gameObject);
        else
            timeoutTimer += Time.deltaTime;

        if (friendly) //at which layer is this abilities attack targeted?
            targetLayer = AbilityController.enemyLayer;
        else
            targetLayer = AbilityController.playerLayer;


        switch (abilityType)
        {
            case Ability.Type.projectile: //if ability is a projectile
                
                transform.Translate(Vector3.forward * speed * Time.deltaTime); //move forwards
                if (Physics.CheckSphere(transform.position, radius, targetLayer) //if collision with ground or enemy
                    || Physics.CheckSphere(transform.position, radius * 0.2f, AbilityController.groundLayer))
                {
                    ApplyDamage(GetCollidedActors()); //if collided, damage all enemies in a certain radius
                    Destroy(this.gameObject);
                }
                break;

            case Ability.Type.explosion: //if ability is an explosion

                if (!dealtDamage)
                {
                    dealtDamage = true;

                    if (Physics.CheckSphere(transform.position, radius, AbilityController.enemyLayer))
                        ApplyDamage(GetCollidedActors()); //damage all enemies in radius
                }
                break;

            case Ability.Type.missile:

                transform.Translate(Vector3.forward * speed * Time.deltaTime); //move forwards
                if (Physics.CheckSphere(transform.position, radius, targetLayer) //if collision with ground or enemy
                    || Physics.CheckSphere(transform.position, radius * 0.2f, AbilityController.groundLayer))
                {
                    AbilityController.CastExplosion(transform.position, damage, 15); //if collided, create an explosion effect (no damage)
                    ApplyDamage(GetCollidedActors());
                    Destroy(this.gameObject);
                }
                break;

            case Ability.Type.shockwave:

                if (!dealtDamage)
                {
                    Stun(GetCollidedObjectsBox(new Vector3(0.5f, 2f, 1f))); //stuns the enemy only once
                    dealtDamage = true;
                }
                else
                {
                    //if (movementTimer <= speed) //destroys the gameobject after the animation has played
                    //    movementTimer += Time.deltaTime;
                    //else
                    //    Destroy(this.gameObject);
                }

                break;

            case Ability.Type.laser:

                if (transform.localScale.z > 0) //scales the laser down in the x and z axes, giving the effect of a narrowing laer beam
                {
                    transform.localScale = new Vector3(
                        transform.localScale.x - (Time.deltaTime * 2.5f),
                        transform.localScale.y,
                        transform.localScale.z - (Time.deltaTime * 2.5f)
                        );
                }
                else
                    Destroy(this.transform.parent.gameObject);

                break;

            default:
                break;
        }
    }

    private List<Actor> GetCollidedActors() //returns list of all actors in a radius
    {
        Collider[] collisions = Physics.OverlapSphere(transform.position, radius, targetLayer); //gets all colliders in radius

        List<Actor> returnActors = new List<Actor>();

        for (int i = 0; i < collisions.Length; i++)
        {
            if (collisions[i].transform.gameObject.GetComponent<Actor>() != null) //if the collider is attached to an actor component, add to list
            {
                returnActors.Add(collisions[i].transform.gameObject.GetComponent<Actor>());
            }
        }

        return returnActors;
    }

    private List<GameObject> GetCollidedObjectsBox(Vector3 bounds) //returns list of all gameobjects in a box
    {
        Collider[] collisions = Physics.OverlapBox(transform.position, bounds, transform.localRotation, targetLayer); //gets all colliders in box

        List<GameObject> returnObjects = new List<GameObject>();

        for (int i = 0; i < collisions.Length; i++)
        {
            if (collisions[i].transform.gameObject.GetComponent<Actor>() != null) //if the collider is attached to an actor component, add to list
            {
                returnObjects.Add(collisions[i].transform.gameObject);
            }
        }

        return returnObjects;
    }

    void ApplyDamage(List<Actor> actors) //applies damage to all actors in a given list
    {
        for (int i = 0; i < actors.Count; i++)
            actors[i].Damage(damage);
    }

    void Stun(List<GameObject> gameObjects) //stuns all actors in a given list
    {
        for (int i = 0; i < gameObjects.Count; i++)
        {
            gameObjects[i].GetComponent<Actor>().Damage(damage);

            if (gameObjects[i].GetComponent<EnemyBehaviour>() != null)
                gameObjects[i].GetComponent<EnemyBehaviour>().stunTimer += stunAmount;
        }
    }
}
