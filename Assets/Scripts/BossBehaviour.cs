﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossBehaviour : MonoBehaviour {

    Actor actor;
    NavMeshAgent agent;
    CapsuleCollider collider;
    Animator animator;
    public GameObject mesh, projectilePortsParent;

    public List<GameObject> projectilePorts = new List<GameObject>();

    public bool jumping = false;
    private bool droppedItem = false;
    bool routineRunning = false;

    public int shotsBeforeReload;
    private int reloadCounter = 1000;

    
    public float DistanceToPlayer { get { return Vector3.Distance(transform.position, PlayerController.playerObject.transform.position); } }
    public float instantKillRange, jumpInterval, jumpDuration, jumpHeight, fireDelay, reloadDelay, attackDistance, attackTimer, attackTimerReset;
    private float jumpIntervalTimer, jumpDurationTimer, initialColliderHeight, initialAgentRadius, initialMeshHeight, fireTimer, reloadTimer, distance;

	// Use this for initialization
	void Start () {
        actor = GetComponent<Actor>();
        agent = GetComponent<NavMeshAgent>();
        collider = GetComponent<CapsuleCollider>();
        animator = GetComponentInChildren<Animator>();


        actor.BossConstructor();
        initialColliderHeight = collider.center.y;
        initialAgentRadius = agent.radius;
        initialMeshHeight = mesh.transform.localPosition.y;
	}
	
	// Update is called once per frame
	void Update () {

        
        if (actor.IsDead && !droppedItem) //if the boss is dead, drop loot item
        {
            EnemyController.DropObject(transform.position, actor.enemyCreature);
            jumping = true;
            ToggleHide();
            droppedItem = true;
            animator.SetTrigger("dead");
            actor.enabled = false;            
            StartCoroutine(Death());
                       
        }

        if (!PlayerController.player.IsDead && !actor.IsDead) //if the player is alive
        {
            //projectilePortsParent.transform.Rotate(new Vector3(0, 40 * Time.deltaTime, 0));

            if (reloadCounter >= shotsBeforeReload) //shoot at specified intervals
            {
                if (reloadTimer >= reloadDelay)
                {
                    reloadCounter = 0;
                    reloadTimer = 0;
                }
                else
                    reloadTimer += Time.deltaTime;
            }
            else
            {
                if (fireTimer >= fireDelay)
                {
                    fireTimer = 0;
                    FireProjectiles();
                }
                else
                    fireTimer += Time.deltaTime;
            }

            if (agent.isOnNavMesh)
                agent.SetDestination(PlayerController.playerObject.transform.position);

            if (Vector3.Distance(transform.position, PlayerController.playerObject.transform.position) <= instantKillRange)
                PlayerController.player.Damage(40);
        }
        else
        {
            jumping = false;
            ToggleHide();
            agent.enabled = false; //freeze
        }
        attackTimer -= Time.deltaTime;
        distance = Mathf.Abs(Vector3.Distance(this.transform.position, PlayerController.playerObject.transform.position));
        if (distance <= attackDistance && attackTimer <= 0 && !actor.IsDead)
        {
            attackTimer = attackTimerReset;
            Attack();
        }

    }

    void ToggleHide() //used to hide boss as it jumps out of the arena, obsolete?
    {
        if (jumping)
        {
            agent.radius = 0f;
            collider.center = new Vector3(0, 800, 0);
            mesh.transform.localPosition = new Vector3(0, 800, 0);

        }
        else
        {
            agent.radius = initialAgentRadius;
            collider.center = new Vector3(0, initialColliderHeight, 0);
            mesh.transform.localPosition = new Vector3(0, initialMeshHeight, 0);
        }
    }

    void FireProjectiles() //fires projectiles from ports
    {
        if (routineRunning == false && !actor.IsDead)
        {
            routineRunning = true;
            if (agent.isOnNavMesh)
                agent.isStopped = true;
            
            StartCoroutine(Fire());
            reloadCounter++;
            for (int i = 0; i < projectilePorts.Count; i++)
            {
                Transform portTransform = projectilePorts[i].transform;
                //AbilityController.CastBossAttack(portTransform.position, portTransform.rotation, GameController.currentMapEnemyLevel * 10 + 10, 18, false);
                AbilityController.CastMissile(portTransform.position, portTransform.rotation, GameController.currentMapEnemyLevel * 10 + 30, 18, false);
            }
            animator.SetTrigger("fire");
        }
    }

    void Attack()
    {
        if(routineRunning == false && !actor.IsDead)
        {
            if (agent.isOnNavMesh)
                agent.isStopped = true;
            animator.SetTrigger("attack");
            routineRunning = true;
            StartCoroutine(Attacking());
            //There is a seperate damage trigger component attached to the claws
        }
    }

    IEnumerator Fire()
    {
        yield return new WaitForSeconds(4.8f);
        if (agent.isOnNavMesh)
            agent.isStopped = false;
        routineRunning = false;
    }

    IEnumerator Attacking()
    {
        yield return new WaitForSeconds(1.5f);
        if (agent.isOnNavMesh)
            agent.isStopped = false;
        routineRunning = false;
    }

    IEnumerator Death()
    {
        yield return new WaitForSeconds(3f);
        BossFightController.fightComplete = true;
        Destroy(gameObject);
    }
}
