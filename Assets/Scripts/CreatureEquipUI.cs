﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CreatureEquipUI : MonoBehaviour {

    public static bool isDisplayed, buttonPressed = false, isDragging = false, shouldRefresh = false;
    private GameObject pannel, stats;

    public static float scrollOffset = 0f;

    [SerializeField]
    float tabDistance;

    [SerializeField]
    Vector3 tabBasePosition;

    [SerializeField]
    GameObject tabObject;

    private List<GameObject> tabs = new List<GameObject>(); //all the tabs in the scene

    // Use this for initialization
    void Start () {
        stats = transform.GetChild(0).gameObject;
        pannel = transform.GetChild(1).gameObject;
        scrollOffset = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        if (shouldRefresh)
        {
            shouldRefresh = false;
            Refresh();
        }

        if (Input.GetAxis("Jump") != 0 && GameController.isPaused == false) //open and close equip menu
        {
            if (!buttonPressed)
            {
                buttonPressed = true;
                isDisplayed = !isDisplayed;
            }
        }
        else
            buttonPressed = false;

        if (isDisplayed)
        {
            scrollOffset -= Input.GetAxis("Mouse ScrollWheel") * 400f;
            if (scrollOffset < 0)
                scrollOffset = 0; //change to clamp that gets max scroll amount
        }

        pannel.SetActive(isDisplayed);
        stats.SetActive(isDisplayed);

        if (isDisplayed)
        {
            for (int i = 0; i < PlayerController.storedCreatures.Count; i++)
            {
                if (PlayerController.storedCreatures.Count > tabs.Count)
                {
                    Vector3 adjustedPosition = new Vector3(tabBasePosition.x, tabBasePosition.y + (tabDistance * tabs.Count), tabBasePosition.z);
                    tabs.Add(Instantiate(tabObject, this.transform.GetChild(1)));
                    tabs[tabs.Count - 1].transform.localPosition = new Vector3(tabBasePosition.x, tabBasePosition.y + (tabDistance * (tabs.Count - 1)), tabBasePosition.z);
                    tabs[tabs.Count - 1].GetComponent<CreatureEquipTab>().dockedPosition = tabs[tabs.Count - 1].transform.localPosition;
                    tabs[tabs.Count - 1].GetComponent<CreatureEquipTab>().creature = PlayerController.storedCreatures[tabs.Count - 1];
                    tabs[tabs.Count - 1].GetComponent<CreatureEquipTab>().index = tabs.Count - 1;
                }
            }
        }
    }

    public void ToggleDisplay()
    {
        isDisplayed = !isDisplayed;
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void Refresh()
    {
        for (int i = 0; i < tabs.Count; i++)
            Destroy(tabs[i]);

        tabs = new List<GameObject>();
    }
}
