﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormationController : MonoBehaviour { //deals with the formations for the creatures

    public static int currentFormation = 0;

    private static int totalFormations;

    public static GameObject formationObject;
    
    class Formation //container for each formation
    {
        public string Name { get { return parentObject.name; } }
        public GameObject parentObject;
        public GameObject[] position = new GameObject[4];

        public Formation(GameObject parent) //constructor loads gameobjects from children
        {
            parentObject = parent;
            for (int i = 0; i < 4; i++)
                position[i] = parentObject.transform.GetChild(i).gameObject;
        }
    }

    private static List<Formation> formations = new List<Formation>(); //list of all loaded formations
    public static Vector3[] position = new Vector3[4]; //the 4 current positions of the current formation

	// Use this for initialization
	void Start () {
        formationObject = this.gameObject;
        GetFormations();
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < 4; i++)
        {
            if (formations[currentFormation].position[i] != null)
                position[i] = formations[currentFormation].position[i].transform.position;
        }
	}

    public static void GetFormations() //loads formations from gameobjects
    {
        formations.Clear();

        totalFormations = formationObject.transform.childCount;
        for (int i = 0; i < totalFormations; i++)
            formations.Add(new Formation(formationObject.transform.GetChild(i).gameObject));
    }

    public static string CycleFormations() //cycles formation and returns new formation name
    {
        GetFormations();

        if (currentFormation < (totalFormations - 1))
            currentFormation++;
        else
            currentFormation = 0;   

        return formations[currentFormation].Name;
    }

    public static string GetName
    {
        get { return formations[currentFormation].Name; }
    }

}
