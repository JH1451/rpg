﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class LevelTransitionVolume : MonoBehaviour { //teleports the player between scenes

    public enum Area { hub, dungeon, boss };
    public Area currentArea = Area.hub;

    [SerializeField]
    bool isHub = false;
    bool trigger = false;
    Vector3 bounds;

    [SerializeField]
    LayerMask playerMask;

	// Use this for initialization
	void Start () {
        bounds = GetComponent<BoxCollider>().size * 0.5f; //loads bounds from attached collider (that is just there for size visualization)
        Destroy(GetComponent<BoxCollider>());

    }

    // Update is called once per frame
    void Update () {
        if (Physics.CheckBox(transform.position, bounds, Quaternion.identity, playerMask)) //if player is inside region
        {
            switch (currentArea)
            {
                case Area.hub:
                    PlayerController.playerSave = new Actor();
                    PlayerController.playerSave.Constructor(PlayerController.player); //saves the players actor class incase they die

                    PlayerController.storedCreaturesSave = new List<Creature>(PlayerController.storedCreatures);
                    //GameController.currentMapEnemyLevel--;

                    for (int i = 0; i < AbilityController.equippedAbilities.Length; i++)
                    {
                        if (AbilityController.equippedAbilities[i].IsEquipped)
                            AbilityController.equippedAbilitiesSave[i] = AbilityController.equippedAbilities[i];
                        else
                            AbilityController.equippedAbilitiesSave[i] = new AbilityController.AbilityEquipSlot();
                    }

                    SceneController.StartSceneLoad("Dungeon"); //loads various scenes
                    break;
                case Area.dungeon:
                    SceneController.StartSceneLoad("Boss");
                    break;
                case Area.boss:
                    SceneController.StartSceneLoad("Hub");
                    break;
                default:
                    break;
            }
        }
    }
}
