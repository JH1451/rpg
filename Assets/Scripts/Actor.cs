﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : MonoBehaviour { //stores information for an actor i.e the player or an enemy

    [SerializeField]
    bool isDead, isFriendly, isBoss, takenDamage;
    [SerializeField]
    int level, health, maxHealth, armor, energy, maxEnergy, healthRecharge, energyRecharge, healingBuffer, enemyAttack;

    #region PROPERTIES
    public bool IsDead { get { return isDead; } }
    public int Health { get { return health; } }
    public int HealedHealth { get { return health + healingBuffer;  } }
    public int MaxHealth { get { return maxHealth; } }
    //public int Money { get { return money; } set { money += value; } }
    public int Energy { get { return energy; } }
    public int EnergyRecharge { get { return energyRecharge; } set { energyRecharge = value; } }
    public int HealthRecharge { get { return energyRecharge; } set { energyRecharge = value; } }
    public int MaxEnergy { get { return maxEnergy; } }
    public int EnemyAttack { get { return enemyAttack; } }
    public int Armor { get { return armor; } set { if (value <= 100) armor = value; } }
    public float ArmorDecimal { get { return (float)armor * 0.01f; } }
    #endregion
    
    float healTimer, rechargeTimer, healIncrementDelay = 0.01f, rechargeIncrementDelay = 1f, materialTimer = 0.3f;
    public Creature enemyCreature;
    CharacterAnimator characterAnimator;

	// Use this for initialization
	void Start () {
        characterAnimator = GetComponent<CharacterAnimator>();

        if (isBoss)
            BossConstructor();
	}
	
	// Update is called once per frame
	void Update () {
        if (healingBuffer > 0)
        {
            if (healingBuffer > (maxHealth - health))
                healingBuffer = (maxHealth - health);

            if (healTimer >= healIncrementDelay) //slowly add healing buffer to main health
            {
                healTimer = 0;
                health++;
                healingBuffer--;

                
            }
            else
                healTimer += Time.deltaTime;
        }

        if (rechargeTimer >= rechargeIncrementDelay) //heals or recharges player
        {
            rechargeTimer = 0;
            ChargeEnergy(EnergyRecharge);
            Heal(HealthRecharge);
        }
        else
            rechargeTimer += Time.deltaTime;

        if (isDead)
        {
            health = 0;
            energy = 0;
        }
            
    }

    public void Constructor() //sets values for default actor (player)
    {
        isFriendly = true;
        health = 150;
        maxHealth = 150;
        armor = 0;
        energy = 100;
        maxEnergy = 100;
        //money = 0;
    }

    public void Constructor(Actor actor) //duplicates actor for saving
    {
        isDead = false;
        isFriendly = true;
        takenDamage = false;
        health = actor.MaxHealth;
        armor = actor.Armor;
        energy = actor.maxEnergy;
        energyRecharge = actor.EnergyRecharge;
        //money = actor.money;
    }

    public void Constructor(int _maxHealth, int _armor, int _money) //sets values for an enemy
    {
        isFriendly = false;
        health = _maxHealth;
        maxHealth = _maxHealth;
        armor = _armor;
        //money = _money;
    }

    public void Constructor(int _level) //generates an enemy from a level input
    {
        isFriendly = false;
        level = _level;

        maxHealth = (level * 25) + Random.Range(-level, level + 1);
        health = maxHealth;
        armor = (level * 4) + Random.Range(-level, level + 1);
        enemyAttack = (level * 5) + Random.Range(-level, level + 1);
        //money = (level * 2) + Random.Range(-level, level + 1);
        float cooldown = 1f;
        int energyCost = level * 2;

        enemyCreature = new Creature(level, this.gameObject.GetComponent<EnemyBehaviour>().type, enemyAttack, armor, energyCost, cooldown);
    }

    public void BossConstructor() //generates boss enemy from a level input
    {
        isFriendly = false;
        level = GameController.currentMapEnemyLevel + 1;

        maxHealth = (level * 250) + Random.Range(-level, level + 1);
        health = maxHealth;
        armor = (level * 5) + Random.Range(-level, level + 1);
        enemyAttack = (level * 10) + Random.Range(-level, level + 1);
        //money = (level * 2) + Random.Range(-level, level + 1);

        float cooldown = (float)level * 0.2f;
        int energyCost = level * 5;

        EnemyController.EnemyType creatureType; //gets a random creature from a selection for the boss to drop
        {
            List<EnemyController.EnemyType> availableBossCreatures = new List<EnemyController.EnemyType>();
            availableBossCreatures.Add(EnemyController.EnemyType.flying1);
            availableBossCreatures.Add(EnemyController.EnemyType.flying2);
            availableBossCreatures.Add(EnemyController.EnemyType.flying3);

            creatureType = availableBossCreatures[Random.Range(0, availableBossCreatures.Count)];
        }

        enemyCreature = new Creature(level + 1, creatureType, enemyAttack, armor, energyCost, cooldown);
        enemyCreature.isSuper = true;
        enemyCreature.name = EnemyController.GetNameFromEnemyType(creatureType, true);

    }

    public void Damage(int hitAmount) //damage the actor
    {
        if (!isDead && !PlayerController.player.IsDead)
        {
            takenDamage = true;
            try
            {
                TextController.CreateText(transform.position, TextController.TextType.damage, hitAmount.ToString());
            } catch { }
            if (GetArmorHit(hitAmount) >= health)
            {
                health = 0;
                Destroyed();
            }
            else
                health -= GetArmorHit(hitAmount);
        }
    }

    int GetArmorHit(int hitAmount) //get the damage remaining if some is blocked by armor
    {
        if (armor >= 100) //if 100% armor, hit for only 1 damage
        {
            return 1;
        }
        else
        {
            float armorReduction = (float)hitAmount * ArmorDecimal;
            float returnValue = Mathf.Ceil((float)hitAmount - armorReduction);
            return (int)returnValue; //reutrn value = original damage - (damage * armor%)
        }
    }

    void Destroyed() //manually destroy the actor
    {
        health = 0;
        isDead = true;
    }

    public void Heal(int amount) //heal specific amount not above maxHealth
    {
        if (!isDead && (health != maxHealth))
        {
            if (amount <= (maxHealth - health))
            {
                healingBuffer += amount; //healed HP is not added directly to the health but to a buffer which slowly adds to heal. healing is not immediate
            }
            else
                healingBuffer += (maxHealth - health);
        }
    }
    
    public void Attack(Actor target, int damage) //deals randomised damage
    {
        if (!isDead && !target.IsDead)
            target.Damage(GetVariedInt(damage, 3));
    }

    public int GetVariedInt(int value, int variance) //returns an integer that is varied by a set amount
    {
        int min = value - variance;
        int max = value + variance + 1;
        if (min <= 0)
            min = 1;
        return Random.Range(min, max);
    }

    public bool UseEnergy(int amount) //spends energy
    {
        if (isDead)
            return false;

        if (amount <= energy)
        {
            energy -= amount;
            return true;
        }
        else return false;
    }

    public void ChargeEnergy(int amount) //replenishes energy
    {
        if (!isDead)
        {
            if ((amount + energy) <= maxEnergy)
                energy += amount;
            else
                energy = maxEnergy;
        }
    }

    void SetMaterial(Material input)
    {
        if (transform.GetChild(0).gameObject.GetComponent<MeshRenderer>() != null)
            transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = input;
    }
}
