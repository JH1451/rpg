﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CreatureBehaviour : MonoBehaviour { //controls the creature gameobjects when they are active
    public Creature creature;

    public bool isAttacking = false, shouldAttack = false;

    [SerializeField]
    private float attackDelay = 0.01f, attackThreshold = 4f;
    private float attackTimer = 0;
    Actor returnActor = new Actor();
    public int currentSlot = 0;
    NavMeshAgent agent, creatureAgent;

    Actor actor;
    
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        agent.stoppingDistance = 0;
    }
	
	void Update () {
        bool matchedCreature = false;
        for (int i = 0; i < AbilityController.equippedAbilities.Length; i++)
        {
            if (AbilityController.equippedAbilities[i].creature == creature)
                matchedCreature = true;
        }
        creature.isEquipped = matchedCreature;
        
        if (!creature.isEquipped)
        {
            Destroy(this.gameObject); //remove this creature from the scene if it's not eqipped
        }
        else
        {
            try
            {
                actor = GetNearestEnemy();
            }
            catch
            { }

            agent.speed = PlayerController.playerObject.GetComponent<NavMeshAgent>().speed; //match players speed

            if (isAttacking && shouldAttack) //obsolete code from when creatures moved and attacked by themselves
            {
                if ((actor != null) && !actor.IsDead)
                {
                    agent.SetDestination(actor.transform.position);
                    if (Vector3.Distance(transform.position, actor.transform.position) <= (agent.stoppingDistance + 1f))
                    {
                        Attack();
                    }
                    else if (Vector3.Distance(transform.position, actor.transform.position) <= attackThreshold)
                        isAttacking = false;
                }
                else
                    isAttacking = false;
            }
            else
            {
                if (agent.isOnNavMesh)
                {
                    agent.SetDestination(FormationController.position[creature.equipIndex]);
                }
            }
        }
	}

    void Attack() //attack an actor with an interval
    {
        if (attackTimer >= attackDelay)
        {
            attackTimer = 0;
            if (!actor.IsDead)
            {
                actor.Damage(5);
            }
            else
            {
                isAttacking = false;
            }
        }
        else
            attackTimer += Time.deltaTime;
    }

    Actor GetNearestEnemy() //finds the nearest enemy actor
    {
        float distance = 10000;
        

        for (int i = 0; i < EnemyController.levelEnemies.Count; i++)
        {
            if (!EnemyController.levelEnemies[i].IsDead)
            {
                float enemyDistance = Vector3.Distance(transform.position, EnemyController.levelEnemies[i].transform.position);
                if (enemyDistance < distance)
                {
                    distance = enemyDistance;
                    returnActor = EnemyController.levelEnemies[i];
                }
            }
        }

        if (distance <= attackThreshold)
            isAttacking = true;
        else
        {
            isAttacking = false;
            return null;
        }

        if (distance == 123456)
            return null;
        else
            return returnActor;
    }

}
