﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour { //controls the loot dropped by enemies

    public enum PickupType { energy, creature };
    public PickupType type = PickupType.energy;

    public int amount = 20;
    public Creature storedCreature = new Creature();

    private float timer = 0;
    private Vector3 startDirection;
    public string name;

    void Start()
    {
        name = storedCreature.name;
    }

    // Update is called once per frame
    void Update () {
        timer += Time.deltaTime * 6;
        transform.GetChild(0).position += new Vector3(0, Mathf.Sin(timer) * 0.025f, 0); 
        transform.rotation = Quaternion.AngleAxis(timer * 15, Vector3.up);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<PlayerController>() != null) //if the player collides with this
        {
            switch (type)
            {
                case PickupType.energy:
                    TextController.CreateText(transform.position, TextController.TextType.energy, "Fuel Cell Collected");
                    collision.gameObject.GetComponent<Actor>().ChargeEnergy(amount);
                    PlayerController.scrap += amount * 2;
                    Used();
                    break;
                case PickupType.creature:
                    TextController.CreateText(transform.position, TextController.TextType.energy, storedCreature.name + " Captured");
                    collision.gameObject.GetComponent<Actor>().ChargeEnergy(amount);
                    PlayerController.storedCreatures.Add(storedCreature);
                    Used();
                    break;
                default:
                    break;
            }
        }
    }

    void Used()
    {
        //add particle effect
        Destroy(this.gameObject);
    }
}
