﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityUI : MonoBehaviour {

    public static Vector3[] abilityBarPositions = new Vector3[4]; //stores the position of each ability on the screen, used for drag to equip feature

    UnityEngine.UI.Image image;

    private enum DisplayType { SelectedAbilityIndicator, CooldownIndicator, CreatureSprite, ActiveSprite, PassiveSprite }
    private enum Map { Ability1 = 0, Ability2 = 1, Ability3 = 2, Ability4 = 3}

    [SerializeField]
    private Map mapping = Map.Ability1;

    [SerializeField]
    private DisplayType display = DisplayType.CooldownIndicator;
    private Vector3 initialSize;

    private Color initialColour;

    // Use this for initialization
    void Start () {
        image = GetComponent<UnityEngine.UI.Image>(); //get attached image

        if (display == DisplayType.CreatureSprite)
            abilityBarPositions[(int)mapping] = transform.position;

        initialSize = image.transform.localScale;
        if (display == DisplayType.SelectedAbilityIndicator)
            image.transform.localScale = Vector3.zero;
        initialColour = image.color;
    }
	
	// Update is called once per frame
	void Update () {

        if (CreatureEquipTab.tabSelected && (display == DisplayType.SelectedAbilityIndicator)) //show selectedAbilityIndicator if player is dragging an ability UI tab
        {
            image.color = Color.yellow;
            transform.localScale = initialSize;
        }
        else if (display == DisplayType.SelectedAbilityIndicator)
        {
            image.color = Color.white;
            transform.localScale = Vector3.zero;
        }

        if (AbilityController.equippedAbilities[(int)mapping].creature.active.abilityType != Ability.Type.nullType) //if the ability exists
        {
            image.color = initialColour;
            switch (display) //what is the element displaying?
            {
                case DisplayType.SelectedAbilityIndicator:
                    {
                        if (AbilityController.AbilityEquipSlot.selectedAbility == (int)mapping) //only show selectedAbilityIndicator if ability is selected
                            transform.localScale = initialSize;
                        else
                            transform.localScale = new Vector3(0f, 0f, 0f);
                    }
                    break;
                case DisplayType.CooldownIndicator: //displays a radially filled sprite to show the abilities cooldown
                    {
                        image.type = UnityEngine.UI.Image.Type.Filled;

                        if (AbilityController.equippedAbilities[(int)mapping].creature.active.CooldownDecimal == 1)
                            image.fillAmount = 0;
                        else
                            image.fillAmount = AbilityController.equippedAbilities[(int)mapping].creature.active.CooldownDecimal;
                    }
                    break;
                case DisplayType.CreatureSprite:
                    {
                        image.sprite = CreatureController.GetAvatarFromType(AbilityController.equippedAbilities[(int)mapping].creature.type); //set sprite to creatures avatar
                    }
                    break;

                case DisplayType.ActiveSprite:
                    image.sprite = AbilityController.GetSpriteFromAbilityType
                        (AbilityController.equippedAbilities[(int)mapping].creature.active.abilityType); //set sprite to creatures avatar
                    break;
                case DisplayType.PassiveSprite:
                    image.sprite = AbilityController.GetSpriteFromAbilityType
                        (AbilityController.equippedAbilities[(int)mapping].creature.passive.abilityType); //set sprite to creatures avatar
                    break;
            }
        }
        else if (display != DisplayType.SelectedAbilityIndicator) //if no creature/ability is assigned to slot, show a default image
        {
            image.color = Color.gray;
            image.sprite = AbilityController.GetSpriteFromAbilityType(Ability.Type.nullType);
        }
    }
}
