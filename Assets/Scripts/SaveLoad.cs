﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;

public class SaveLoad : MonoBehaviour //deals with saving and loading the players progress from a file
{
    public void Save()
    {
        using (StreamWriter sw = new StreamWriter("Assets/Save.txt"))
        {
            sw.WriteLine(PlayerController.scrap + "-");
            for (int i = 0; i < PlayerController.storedCreatures.Count; i++)
            {
                sw.WriteLine(PlayerController.storedCreatures[i].level + "-" + PlayerController.storedCreatures[i].type + "-" + PlayerController.storedCreatures[i].active.abilityType + "-" + PlayerController.storedCreatures[i].passive.abilityType + "-" + PlayerController.storedCreatures[i].active.damage + "-" + PlayerController.storedCreatures[i].passive.passiveValue + "-" + PlayerController.storedCreatures[i].active.energyCost + "-" + PlayerController.storedCreatures[i].active.cooldown + "-.");
            }
            sw.WriteLine("!");
        }
    }

    public void Load()
    {
        int level, damage, energyCost, scrap;
        float cooldown, passiveValue;
        string creatureType, activeType, passiveType;


        StreamReader stream = new StreamReader("Assets/Save.txt");
        PlayerController.storedCreatures.Clear();
        scrap = GetInteger(ref stream);
        stream.Read();
        
        PlayerController.scrap = scrap;
        while (!stream.EndOfStream)
        {
            level = GetInteger(ref stream);
            creatureType = GetWord(ref stream);
            activeType = GetWord(ref stream);
            passiveType = GetWord(ref stream);
            damage = GetInteger(ref stream);
            passiveValue = GetFloat(ref stream);
            energyCost = GetInteger(ref stream);
            cooldown = GetFloat(ref stream);
                EnemyController.EnemyType enumCreatureType = (EnemyController.EnemyType)System.Enum.Parse(typeof(EnemyController.EnemyType), creatureType);
                Ability.Type enumActiveType = (Ability.Type)System.Enum.Parse(typeof(Ability.Type), activeType);
                Ability.Type enumPassiveType = (Ability.Type)System.Enum.Parse(typeof(Ability.Type), passiveType);
                PlayerController.storedCreatures.Add(new Creature(level, enumCreatureType, enumActiveType, enumPassiveType, damage, passiveValue, energyCost, cooldown));
                stream.Read();
            stream.Read();
            stream.Read();
            if ((stream.Peek() < 48) || (stream.Peek() > 57 && stream.Peek() < 97) || (stream.Peek() > 122))
            {
                stream.ReadToEnd();
                break;
            }

        }
        
    }

    string GetWord(ref StreamReader stream)     //gets word from text file
    {
        string word = "";
        while (stream.Peek() != 45)             //whilst character is not -
            word += (char)stream.Read();        //append char to end of word
        stream.Read();
        return word;
    }

    int GetInteger(ref StreamReader stream)
    {
        string integer = "";

            while ((stream.Peek() != 45))       //whilst character is not - 
            {
                integer += (char)stream.Read();                                                 //append char to end of integer string
            }
            stream.Read();
        
        
        return Convert.ToInt32(integer);
    }

    float GetFloat(ref StreamReader stream)
    {
        string integer = "";


            while ((stream.Peek() != 45))       //whilst character is not - 
            {
                integer += (char)stream.Read();                                                 //append char to end of integer string
            }
            stream.Read();
        
            float result;
            
            float.TryParse(integer, out result);
        
        return (result);
    }
}
