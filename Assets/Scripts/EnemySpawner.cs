﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Threading;

public class EnemySpawner : MonoBehaviour { //spawns enemies into the game, either on start or via proximity

    [SerializeField]
    [Tooltip("Distance from player that enemies spawn")]
    private float activationDistance = 10f, spawnDelay = 15f;
    [SerializeField]
    private bool spawnDistance = false, alreadySpawned = false, isBoss = false;
    [SerializeField]
    [Tooltip("List of enemy prefabs to be randomly selected from")]
    List<GameObject> enemies = new List<GameObject>();
    [SerializeField]
    [Tooltip("The percentage chance that the enemy will be capturable")]
    private int captureChance = 70;
    private float spawnDelayTimer = 0;

    [SerializeField]
    int xRad, zRad;
    [SerializeField]
    List<int> spawnNumbers = new List<int>();
    private int enemyType;
    private int enemiesSpawned = 0;
    private Vector3 spawnLocation;

    private void Update()
    {
        if (!isBoss)
        {
            if (!alreadySpawned)
            {
                if ((!spawnDistance || Vector3.Distance(transform.position, PlayerController.playerObject.transform.position) <= activationDistance))
                {
                    SpawnEnemies();
                }
            }
        }
        else
        {
            if (spawnDelayTimer >= spawnDelay)
            {
                spawnDelayTimer = 0;
                SpawnEnemiesBoss();
            }
            else
                spawnDelayTimer += Time.deltaTime;
        }
    }

    void SpawnEnemies()
    {
        for (int spawnType = 0; spawnType < spawnNumbers.Count; spawnType++)
        {
            enemyType = Random.Range(0, enemies.Count);
            for (int i = 0; i < spawnNumbers[spawnType]; i++)
            {
                spawnLocation = transform.position + new Vector3(Random.Range(-xRad, xRad), 0, Random.Range(-zRad, zRad));
                GameObject enemyObject = Instantiate(enemies[enemyType], spawnLocation, Quaternion.identity);
                enemyObject.GetComponent<Actor>().Constructor(GameController.currentMapEnemyLevel);
                enemyObject.GetComponent<EnemyBehaviour>().capturable = Random.Range(0, 101) > captureChance;
                enemiesSpawned++;
            }
        }
        alreadySpawned = true;
    }

    void SpawnEnemiesBoss()
    {
        GameObject enemyObject = Instantiate(enemies[Random.Range(0, enemies.Count)], transform.position, Quaternion.identity);
        enemyObject.GetComponent<Actor>().Constructor(GameController.currentMapEnemyLevel);
        enemyObject.GetComponent<EnemyBehaviour>().capturable = false;
        enemyObject.GetComponent<NavMeshAgent>().speed *= 3;
    }
}
