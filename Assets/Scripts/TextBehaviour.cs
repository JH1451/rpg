﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextBehaviour : MonoBehaviour { //moves and fades the text that is printed to the screen by TextController

    UnityEngine.UI.Text textComponent;

	// Use this for initialization
	void Start () {
        textComponent = GetComponent<UnityEngine.UI.Text>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.Translate(0, Time.deltaTime * 80, 0); //move upwards

        Color textColour = textComponent.color;
        textComponent.color = new Color(textColour.r, textColour.g, textColour.b, textColour.a - Time.deltaTime); //fade out

        if (textColour.a <= 0)
            Destroy(this.gameObject); //if fully faded then destroy
	}
}
