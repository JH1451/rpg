﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UpgradeController : MonoBehaviour { //controlls creature upgrade and dismatle process

    public bool upgradeScreenOpen = false, confirmScreenOpen = false;
    public static bool upgradeScreenOpenStatic = false;

    public GameObject upgradeScreenUI;
    public GameObject dismantleConfirmScreen;

    public UnityEngine.UI.Button upgradeButton;
    public UnityEngine.UI.Button dismantleButton;
    public UnityEngine.UI.Text upgradeText;
    public UnityEngine.UI.Text dismantleText;

    public static Creature selectedCreature;

    public static bool CreatureSelected
    { get { return (selectedCreature != null) && (selectedCreature.type != EnemyController.EnemyType.nullType); } }

    public static int CurrentCost
    { get { return GameController.currentMapEnemyLevel * 120; } }
    public static int CurrentRefund
    { get { return CurrentCost / 2; } }
    

	// Update is called once per frame
	void Update () {

        upgradeText.text = "Upgrade [-" + CurrentCost + "]";
        dismantleText.text = "Dismantle [-" + CurrentRefund + "]";
        
        if (upgradeScreenOpen && (Input.GetAxis("Jump") != 0)) //closes upgrade screen if space is pressed
            SetUpgradeScreenActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        upgradeScreenUI.SetActive(upgradeScreenOpen);
        if (!upgradeScreenOpen) confirmScreenOpen = false;
        dismantleConfirmScreen.SetActive(confirmScreenOpen);

        upgradeButton.interactable = CreatureSelected;
        dismantleButton.interactable = CreatureSelected;
    }

    public void UpgradeButtonPressed()
    {
        if (CreatureSelected)
        {
            if (PlayerController.scrap >= CurrentCost) //add dynamic value
            {
                PlayerController.scrap -= CurrentCost;
                selectedCreature.level++;
                UpgradeAbility(1.3f, ref selectedCreature.active);
                UpgradeAbility(1.3f, ref selectedCreature.passive);
            }
            else
                TextController.CreateText(PlayerController.playerObject.transform.position, TextController.TextType.energy, "Not enough scrap!");
        }
    }

    public void CloseButtonPressed()
    {
        SetUpgradeScreenActive(false);
    }

    public void DismantleButtonPressed() //opens confirm screen
    {
        confirmScreenOpen = !confirmScreenOpen;
    }

    public void DismantleConfirmButtonPressed() //dismantles (destroys) specific creature
    {
        if (CreatureSelected)
        {
            AbilityController.UnequipAbility(selectedCreature);
            PlayerController.storedCreatures.Remove(selectedCreature);
            selectedCreature = null;
            CreatureEquipUI.shouldRefresh = true;
            PlayerController.scrap += CurrentRefund;
            TextController.CreateText(PlayerController.playerObject.transform.position, 
                TextController.TextType.energy, "[+" + CurrentRefund + " Scrap]");
            confirmScreenOpen = false;
        }
    }

    public void UpgradeAbility(float scaleFactor, ref Ability ability) //upgrades a creatures active ability
    {
        float upgradedDamage = ability.damage * scaleFactor;
        ability.damage = Mathf.RoundToInt(upgradedDamage);
    }

    public void SetUpgradeScreenActive(bool active)
    {
        upgradeScreenOpen = active;
        upgradeScreenOpenStatic = active;
        upgradeScreenUI.SetActive(active);
        CreatureEquipUI.isDisplayed = active;
    }

    public void ToggleUpgradeScreen()
    {
        upgradeScreenOpen = !upgradeScreenOpen;
        upgradeScreenOpenStatic = upgradeScreenOpen;
        upgradeScreenUI.SetActive(upgradeScreenOpen);
        CreatureEquipUI.isDisplayed = upgradeScreenOpen;
    }
}
