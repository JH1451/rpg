﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public GameObject pauseMenu;

	public void Resume()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        GameController.isPaused = false;
        AudioListener.pause = false;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
