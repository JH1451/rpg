﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFightController : MonoBehaviour {

    public GameObject bossObject, portal;

    public static bool fightComplete = false;

    private Actor bossActor;

	// Use this for initialization
	void Start () {
        bossActor = bossObject.GetComponent<Actor>();
	}
	
	// Update is called once per frame
	void Update () {
        if (fightComplete)
        {
            portal.SetActive(true); //opens the teleporter when the boss fight ends
        }
	}
}
