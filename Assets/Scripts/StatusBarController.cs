﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusBarController : MonoBehaviour { //scales status bars for health and energy

    UnityEngine.UI.Image image;

    [SerializeField]
    private bool isLowHealthIndicator = false, isOverclockIndicator = false;

    [SerializeField]
    [Tooltip("Speed the bar should scroll")]
    private float speed = 5f;
    private float currentVelocity, targetAmount, sineTimer;
    private enum ValueType { health, energy, healthBuffer };
    [SerializeField]
    [Tooltip("Value to display")]
    private ValueType value = ValueType.health;

	// Use this for initialization
	void Start () {
        image = GetComponent<UnityEngine.UI.Image>();
    }
	
	// Update is called once per frame
	void Update () {

        if (isLowHealthIndicator)
        {
            float alpha = (float)PlayerController.player.Health / (float)PlayerController.player.MaxHealth; //get the decimal of how much health the player has
            alpha = (1 - alpha); //invert

            if (alpha < 0.65f) //does the player have less than 35 health?
                alpha = 0;
            else
            {
                sineTimer += Time.deltaTime * 5;
                alpha *= Mathf.Pow(Mathf.Sin(sineTimer), 2); //pulsate the value via a sin(x)^2 wave 
                alpha -= 0.2f;
            }

            if (PlayerController.player.IsDead) //if the player is dead stop any pulsation
                alpha = 0.8f;
            
            image.color = SetAlpha(image.color, Mathf.SmoothDamp(image.color.a, alpha, ref currentVelocity, speed)); //smoothly set the opacity with the alpha variable
        }
        else if (isOverclockIndicator)
        {
            float alpha = 0;

            if ((Input.GetAxis("Overclock") != 0) && (PlayerController.player.Energy > 0)) //shows effect if overclock is enabled
                alpha = 0.2f;

            image.color = SetAlpha(image.color, Mathf.SmoothDamp(image.color.a, alpha, ref currentVelocity, speed)); //smoothly set the opacity with the alpha variable
        }
        else
        {
            switch (value)  //get the value for how much the bar should be filled
            {
                case ValueType.health:
                    targetAmount = ((float)PlayerController.player.Health / (float)PlayerController.player.MaxHealth);
                    break;
                case ValueType.energy:
                    targetAmount = ((float)PlayerController.player.Energy / (float)PlayerController.player.MaxEnergy);
                    break;
                case ValueType.healthBuffer:
                    targetAmount = ((float)PlayerController.player.HealedHealth / (float)PlayerController.player.MaxHealth);
                    break;
                default:
                    break;
            }

            //smoothly interpolates to the new value
            image.fillAmount = Mathf.SmoothDamp(image.fillAmount, targetAmount, ref currentVelocity, speed * Time.deltaTime);
        }
	}

    private Color SetAlpha(Color colour, float newAlpha) //returns the colour with a modified alpha value
    {
        return new Color(colour.r, colour.g, colour.b, newAlpha);
    }
}
