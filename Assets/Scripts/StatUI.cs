﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatUI : MonoBehaviour { //shows text readouts of player stats

    UnityEngine.UI.Text text;

    public enum Value { health, energy, armor, energyRecharge, scrap};
    public Value displayValue = Value.health;

    private void Start()
    {
        text = GetComponent<UnityEngine.UI.Text>();
    }

    void Update () {
        switch (displayValue)
        {
            case Value.health:
                text.text = PlayerController.player.Health.ToString();
                break;
            case Value.energy:
                text.text = PlayerController.player.Energy.ToString();
                break;
            case Value.armor:
                text.text = "Armor [" + PlayerController.player.Armor + "%]";
                break;
            case Value.energyRecharge:
                text.text = "Recharge [" + PlayerController.player.EnergyRecharge + "/s]";
                break;
            case Value.scrap:
                text.text = "Scrap Metal [" + PlayerController.scrap + "]";
                break;
            default:
                break;
        }
    }
}
