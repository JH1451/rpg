﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonUI : MonoBehaviour {

    public enum Function { toggleFormation }
    public Function buttonFunction = Function.toggleFormation;

    public UnityEngine.UI.Text buttonText;

    private bool textSet = false;

    private void LateUpdate()
    {
        if (!textSet)
        {
            switch (buttonFunction)
            {
                case Function.toggleFormation:
                    buttonText.text = "Formation: " + FormationController.GetName;
                    break;
                default:
                    break;
            }
            textSet = true;
        }
    }

    public void ButtonPressed() //calls for formation to cycle as well as changes the text on the button
    {
        switch (buttonFunction)
        {
            case Function.toggleFormation:
                buttonText.text = "Formation: " + FormationController.CycleFormations();
                break;
            default:
                break;
        }
    }
}
