﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature { //container for information relating to each specific creature

    public Ability active, passive;
    public EnemyController.EnemyType type;
    public bool isEquipped = false, isSuper = false;
    public string name = "Null";
    public int level, equipIndex = 0;
    private float experience;
    public CreatureBehaviour behaviour;

    public Creature()
    {
        active = new Ability();
        passive = new Ability();
    }

    public Creature (int levelInput, EnemyController.EnemyType inputType, int attack, float armor, int energyCost, float cooldown) //constructs a creature
    {
        type = inputType;
        level = levelInput;
        active = GetActive(attack, energyCost, cooldown);
        passive = GetPassive(armor);
        name = EnemyController.GetNameFromEnemyType(inputType, isSuper);
    }

    public Creature(int levelInput, EnemyController.EnemyType inputType, Ability.Type activeType, Ability.Type passiveType, int damage, float armor, int cost, float cooldown) //constructs a creature
    {
        type = inputType;
        level = levelInput;
        active = new Ability(activeType, damage, cost, cooldown);
        passive = new Ability(passiveType, armor);
        name = EnemyController.GetNameFromEnemyType(inputType, isSuper);
    }

    private Ability GetPassive(float armorInput) //generates a passive ability for the creature
    {
        Ability returnAbility = new Ability();
        
        returnAbility.abilityType = (Ability.Type)Random.Range(GetAbilityTypeRange(true), 0); //gets a random passive ability

        switch (returnAbility.abilityType) //change values based on ability type
        {
            case Ability.Type.armorBuff:
                returnAbility.passiveValue = armorInput;
                break;
            case Ability.Type.energyBuff:
                returnAbility.passiveValue = 3;
                break;
            case Ability.Type.healthBuff:
                returnAbility.passiveValue = 2;
                break;
            default:
                break;
        }

        return returnAbility;
    }

    private Ability GetActive(int attackInput, int costInput, float cooldownInput) //generates an active ability for the creature
    {
        Ability returnAbility = new Ability();

        List<Ability.Type> availableAbilityTypes = new List<Ability.Type>();
        availableAbilityTypes = EnemyController.GetAbilitiesForEnemyType(type); //get abilities that are compatable with this enemy type

        returnAbility.abilityType = availableAbilityTypes[Random.Range(0, availableAbilityTypes.Count)]; //pick randomly from compatable abilities

        returnAbility.damage = attackInput;
        returnAbility.energyCost = costInput;
        returnAbility.cooldown = cooldownInput;


        switch (returnAbility.abilityType) //fine tune values specifically based on type
        {
            case Ability.Type.projectile: //if its a projectile, halve the cooldown and damage etc..
                returnAbility.cooldown *= 3f;
                returnAbility.damage *= 3;
                returnAbility.energyCost *= 5;
                break;
            case Ability.Type.explosion:
                returnAbility.cooldown *= 15;
                returnAbility.damage *= 15;
                returnAbility.radius *= 4;
                returnAbility.energyCost *= 20;
                break;
            case Ability.Type.missile:
                returnAbility.cooldown *= 10;
                returnAbility.damage *= 10;
                returnAbility.energyCost *= 15;            
                break;
            case Ability.Type.shockwave:
                returnAbility.cooldown *= 1;
                returnAbility.damage = (int)Mathf.Ceil((float)returnAbility.damage / 2f);
                break;
            case Ability.Type.laser:
                returnAbility.damage = (int)Mathf.Ceil((float)returnAbility.damage / 3f);
                returnAbility.cooldown = 0.075f;
                returnAbility.energyCost /= 2;

                break;
            default:
                break;
        }
        
        return returnAbility;
    }

    int GetAbilityTypeRange(bool isPassive)//gets the highest or lowest value for the ability
    {
        int returnValue = 0;

        if (isPassive)
        {
            foreach (int i in Ability.Type.GetValues(typeof(Ability.Type)))
            {
                if (i < returnValue)
                    returnValue = i;
            }
        }
        else
        {
            foreach (int i in Ability.Type.GetValues(typeof(Ability.Type)))
            {
                if (i > returnValue)
                    returnValue = i;
            }
            returnValue++;
        }

        return returnValue;
    }
}
