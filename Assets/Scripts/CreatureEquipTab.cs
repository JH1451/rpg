﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//controls how the tabs on the creature screen look and act
public class CreatureEquipTab : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IDragHandler, IPointerUpHandler, IEndDragHandler {

    public static bool tabSelected;
    public bool mouseOver;
    public GameObject activeAbilityOverlay, passiveAbilityOverlay;
    public bool isDragging = false;
    public Vector3 dockedPosition, startScale;
    public Creature creature;
    public UnityEngine.UI.Text creatureName, damage, damageOverlay, activeText, activeTextOverlay, passiveText, 
        passiveTextOverlay, passiveValue, passiveValueOverlay, cooldown, energy, activeDesc, passiveDesc;
    public UnityEngine.UI.Image avatar, activeSprite, activeSpriteOverlay, passiveSprite, passiveSpriteOverlay, border, background;
    Vector3 currentVelocity = new Vector3();

    public int index;
    public static int selectedTabIndex = 0;
    private bool ThisTabSelected { get { return index == selectedTabIndex; } }

    private Transform inPanel, abovePanel;
    // Use this for initialization
    void Start () {
        startScale = transform.localScale;
        background = GetComponent<UnityEngine.UI.Image>();
        inPanel = transform.parent;
        abovePanel = inPanel.parent;

        dockedPosition = transform.position;
    }

    void LateUpdate()
    {
        if (mouseOver)
            transform.parent = abovePanel;
        else
            transform.parent = inPanel;

        if (mouseOver && !isDragging && ThisTabSelected) //show or hide overlay screens if mouse is over ability sprite
        {
            if (PlayerController.mouseCurrentlyOver == PlayerController.MouseOverAbility.active)
            {
                passiveAbilityOverlay.SetActive(false);
                activeAbilityOverlay.SetActive(true);
            }

            if (PlayerController.mouseCurrentlyOver == PlayerController.MouseOverAbility.passive)
            {
                activeAbilityOverlay.SetActive(false);
                passiveAbilityOverlay.SetActive(true);
            }
        }
        else
        {
            activeAbilityOverlay.SetActive(false);
            passiveAbilityOverlay.SetActive(false);
        }
        

        creatureName.text = "Level " + creature.level + " " + creature.name; //configure the text for this ability tab
        damage.text = "Damage: " + creature.active.damage;
        damageOverlay.text = damage.text;
        cooldown.text = "Cooldown: " + (creature.active.cooldown * 1000) + " ms";
        activeText.text = creature.active.Name;
        activeTextOverlay.text = activeText.text;
        passiveText.text = creature.passive.Name;
        passiveTextOverlay.text = passiveText.text;
        energy.text = "Energy cost: " +  creature.active.energyCost + " kWh";
        activeDesc.text = creature.active.Desc;
        passiveDesc.text = creature.passive.Desc;

        if (creature.passive.abilityType == Ability.Type.energyBuff)
            passiveValue.text = "Recharge rate: " + creature.passive.passiveValue + " kWh/s";
        else
            passiveValue.text = "Armor strength: " + creature.passive.passiveValue + "%";

        switch (creature.passive.abilityType)
        {
            case Ability.Type.armorBuff:
                passiveValue.text = "Armor strength: " + creature.passive.passiveValue + "%";
                break;
            case Ability.Type.energyBuff:
                passiveValue.text = "Recharge rate: " + creature.passive.passiveValue + " kWh/s";
                break;
            case Ability.Type.healthBuff:
                passiveValue.text = "Repair rate: " + creature.passive.passiveValue + " hp/s";
                break;
            default:
                break;
        }

        passiveValueOverlay.text = passiveValue.text;


        if (isDragging)
            background.color = new Color(background.color.r, background.color.g, background.color.b, 0.2f);
        else
            background.color = new Color(background.color.r, background.color.g, background.color.b, 1f);

        avatar.sprite = CreatureController.GetAvatarFromType(creature.type);
        activeSprite.sprite = AbilityController.GetSpriteFromAbilityType(creature.active.abilityType);
        activeSpriteOverlay.sprite = activeSprite.sprite;
        passiveSprite.sprite = AbilityController.GetSpriteFromAbilityType(creature.passive.abilityType);
        passiveSpriteOverlay.sprite = passiveSprite.sprite;

        if (creature.isEquipped)
            border.color = Color.green;
        else
            border.color = Color.white;

        CreatureEquipUI.isDragging = isDragging;

        if (isDragging)
        {
            transform.localScale = startScale * 0.7f;
        }
        else
        {
            transform.localScale = startScale;

            Vector3 adjustedPosition = dockedPosition;
            adjustedPosition.y += CreatureEquipUI.scrollOffset;

            if (mouseOver)
            {
                Vector3 offset = new Vector3(20f, 0, 0);
                transform.position = Vector3.SmoothDamp(transform.position, adjustedPosition + offset, ref currentVelocity, 5 * Time.deltaTime);
            }
            else
                transform.position = Vector3.SmoothDamp(transform.position, adjustedPosition, ref currentVelocity, 5 * Time.deltaTime);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseOver = true;
        CreatureEquipUI.isDragging = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isDragging = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isDragging = false;
        tabSelected = false;
        if (!UpgradeController.upgradeScreenOpenStatic)
        {
            if (GetNearestAbility(eventData) != -1)
            {
                creature.equipIndex = GetNearestAbility(eventData);
                AbilityController.EquipAbility(GetNearestAbility(eventData), creature);
            }
        }
        else
        {
            UpgradeController.selectedCreature = creature;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!isDragging)
            mouseOver = false;
    }

    public void OnDrag(PointerEventData eventData) //moves the tab when its being dragged
    {
        isDragging = true;
        tabSelected = true;
        transform.parent = abovePanel;
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
        tabSelected = false;
        mouseOver = false;
        transform.parent = inPanel;
    }

    int GetNearestAbility(PointerEventData eventData) //finds the nearest ability slot on the screen
    {
        float distance = 70f;
        int returnIndex = -1;
        for (int i = 0; i < AbilityUI.abilityBarPositions.Length; i++)
        {
            if (Vector3.Distance(AbilityUI.abilityBarPositions[i], eventData.position) < distance)
            {
                returnIndex = i;
                distance = Vector3.Distance(AbilityUI.abilityBarPositions[i], eventData.position);
            }
        }
        return returnIndex;
    }
}
