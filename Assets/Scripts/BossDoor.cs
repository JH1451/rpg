﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoor : MonoBehaviour {

    public Vector3 displacement;
    private Vector3 targetPosition, currentVelocity; //opens a door in the boss arena when the boss is defeated, obsolete?

    public float smoothTime;

	// Use this for initialization
	void Start () {
        targetPosition = transform.position + displacement;
	}
	
	// Update is called once per frame
	void Update () {
        if (BossFightController.fightComplete)
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref currentVelocity, smoothTime);
	}
}
