﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public bool isOrthographic = false;

    [SerializeField]
    private Vector3 displacement;
    private Vector3 currentVelocity;
    [SerializeField]
    private float smoothTime = 10f;

    float minFov, maxFov, sensitivity, minOrtho, maxOrtho;


    public static Vector3 position;

    public Material deathScreenMaterial;

    Quaternion perspectiveRotation = Quaternion.Euler(60, 0, 0);
    Quaternion orthographicRotation = Quaternion.Euler(60, -40, 0);

    Camera cam;

    void Start()
    {
        cam = GetComponent<Camera>();
        minFov = 20f;
        maxFov = 60f;
        sensitivity = 10f;
        minOrtho = 5f;
        maxOrtho = 12f;
    }

    void Update() //controls various camera movement and adjustments
    {
        if (!CreatureEquipUI.isDisplayed)
        {
            float fov = GetComponent<Camera>().fieldOfView;
            fov -= Input.GetAxis("Mouse ScrollWheel") * sensitivity;
            fov = Mathf.Clamp(fov, minFov, maxFov);
            GetComponent<Camera>().fieldOfView = fov;
        }

        if (!CreatureEquipUI.isDisplayed && isOrthographic == true)
        {
            cam.orthographicSize -= (Input.GetAxis("Mouse ScrollWheel"));
        }

        if (Input.GetKeyDown(KeyCode.C) && isOrthographic == false)
        {
            isOrthographic = true;
        }
        else if (Input.GetKeyDown(KeyCode.C) && isOrthographic == true)
        {
            isOrthographic = false;
        }

    }

    void LateUpdate()
    {
        Vector3 TargetPosition = PlayerController.playerObject.transform.position + displacement; //gets target camera position

        GetComponent<Camera>().orthographic = isOrthographic;
        if (isOrthographic)
        {
            transform.localRotation = orthographicRotation;
            TargetPosition.x += 9;
            TargetPosition.y += 5;

            if (!CreatureEquipUI.isDisplayed) //zooms the camera in and out
            {
                cam.orthographicSize -= (Input.GetAxis("Mouse ScrollWheel")) * sensitivity;
                cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, minOrtho, maxOrtho);
            }


        }
        else
            transform.localRotation = perspectiveRotation;



        transform.position = Vector3.SmoothDamp(transform.position, TargetPosition, ref currentVelocity, smoothTime * Time.deltaTime); //smoothly moves camera
        position = transform.position;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination) //adds an effect to the screen when the player dies
    {
        if (PlayerController.player.IsDead) //apply shader if player is dead
            Graphics.Blit(source, destination, deathScreenMaterial);
        else
            Graphics.Blit(source, destination);
    }
}
