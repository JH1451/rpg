﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeUI : MonoBehaviour { //deals with upgrade screen UI
    
    public enum UIComponent { Avatar, Active, Passive, Level, CurrentScrap, Name, Type, Damage, PassiveType, PassiveValue }
    public UIComponent component;

    private bool valuesStale = false;
    private string originalText;
    private Sprite originalSprite;

	// Use this for initialization
	void Start () {
        if (GetComponent<UnityEngine.UI.Text>() != null)
            originalText = GetComponent<UnityEngine.UI.Text>().text;

        if (GetComponent<UnityEngine.UI.Image>() != null)
            originalSprite = GetComponent<UnityEngine.UI.Image>().sprite;
	}
	
	// Update is called once per frame
	void Update () {
        if (UpgradeController.CreatureSelected) //display various images and stats for a specific creature
        {
            switch (component)
            {
                case UIComponent.Avatar:
                    GetComponent<UnityEngine.UI.Image>().sprite = CreatureController.GetAvatarFromType(UpgradeController.selectedCreature.type);
                    break;
                case UIComponent.Active:
                    GetComponent<UnityEngine.UI.Image>().sprite = AbilityController.GetSpriteFromAbilityType(UpgradeController.selectedCreature.active.abilityType);
                    break;
                case UIComponent.Passive:
                    GetComponent<UnityEngine.UI.Image>().sprite = AbilityController.GetSpriteFromAbilityType(UpgradeController.selectedCreature.passive.abilityType);
                    break;
                case UIComponent.Level:
                    int level = UpgradeController.selectedCreature.level;
                    GetComponent<UnityEngine.UI.Text>().text = "Level: " + level + " - > " + (level + 1);
                    break;
                case UIComponent.CurrentScrap:
                    GetComponent<UnityEngine.UI.Text>().text = "Scrap: " + PlayerController.scrap;
                    break;
                case UIComponent.Name:
                    GetComponent<UnityEngine.UI.Text>().text = UpgradeController.selectedCreature.name;
                    break;
                case UIComponent.Type:
                    GetComponent<UnityEngine.UI.Text>().text = UpgradeController.selectedCreature.active.Name;
                    break;
                case UIComponent.Damage:
                    int damage = UpgradeController.selectedCreature.active.damage;
                    GetComponent<UnityEngine.UI.Text>().text = "Damage: " + damage + " - > " + (damage * 2);
                    break;
                case UIComponent.PassiveType:
                    GetComponent<UnityEngine.UI.Text>().text = UpgradeController.selectedCreature.passive.Name;
                    break;
                case UIComponent.PassiveValue:
                    float value = UpgradeController.selectedCreature.passive.passiveValue;
                    GetComponent<UnityEngine.UI.Text>().text = "Value: " + value + " - > " + (value * 2);
                    break;
                default:
                    break;
            }
        }
        else //if no creature is selected, show default text and sprite
        {
            if (GetComponent<UnityEngine.UI.Text>() != null)
                GetComponent<UnityEngine.UI.Text>().text = originalText;

            if (GetComponent<UnityEngine.UI.Image>() != null)
                GetComponent<UnityEngine.UI.Image>().sprite = originalSprite;
        }
    }
}
