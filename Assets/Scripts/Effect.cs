﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour { //script to control behaviour for various visual effects

    private enum EffectType { movementIndicator }
    [SerializeField]
    EffectType Type = EffectType.movementIndicator;
    [SerializeField]
    Vector3 startSize;

    GameController gc;

	// Use this for initialization
	void Start () {
        if (Type == EffectType.movementIndicator)
            transform.localScale = startSize;
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.localScale.magnitude > 0.1f)
            transform.localScale -= (Vector3.one * Time.deltaTime);
        else
            Destroy(this.gameObject);

	}
}
