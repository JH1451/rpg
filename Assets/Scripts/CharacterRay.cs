﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRay : MonoBehaviour {

    int layerMask;

	// Use this for initialization
	void Start () {
        layerMask = LayerMask.GetMask("Ignore Raycast");
	}
	
	// Update is called once per frame
	void Update () {

        Ray ray = new Ray(transform.position + Vector3.up, transform.forward);

        RaycastHit hit;
        Physics.Raycast(ray, out hit, 1000, layerMask);
        
        if (hit.transform.tag == "Minimap") //shows minimap sprites when the player accesses them
            hit.transform.GetComponentInChildren<SpriteRenderer>().enabled = true;
        
        
		
	}
}
