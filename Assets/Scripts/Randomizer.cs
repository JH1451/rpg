﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomizer : MonoBehaviour {
    [SerializeField]
    float minSize = 0.6f, maxSize = 1.8f;
    public List<Material[]> matList = new List<Material[]>();
    [SerializeField]
    Material[] mat;
    [SerializeField]
    Material[] mat2;
    [SerializeField]
    Material[] mat3;
    //public Material mat2;
    public Renderer rend;
    // Use this for initialization
    void Start () {
        matList.Add(mat);

        if (mat2 != null)
            matList.Add(mat2);

        if (mat3 != null)
            matList.Add(mat3);

        Transform[] allChildren = GetComponentsInChildren<Transform>();
        int matNum = Random.Range(0, matList.Count);
        foreach (Transform child in allChildren)
        {
            try
            {
                
                Transform[] childChildren = GetComponentsInChildren<Transform>();
                foreach (Transform childChild in childChildren)
                {
                    rend = child.GetComponent<Renderer>();
                    var matName = rend.material.name;
                    var matNameOnly = matName.Substring(0, 1);

                    rend.material = matList[matNum][int.Parse(matNameOnly) - 1];
                }
            }
            catch { }
        }


        float size = Random.Range(minSize, maxSize);
        this.transform.localScale = new Vector3(size, size, size);

        }
}
