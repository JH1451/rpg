﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public static int currentMapEnemyLevel = 0;

    [SerializeField]
    Material defaultMaterialInspector, hitMaterialInspector, deadMaterialInspector;
    public static Material defaultMaterial, hitMaterial, deadMaterial;

    [SerializeField]
    GameObject pauseMenu, deathScreen;
    public static bool isPaused;

    float deathInputTimer = 0;

    // Use this for initialization
    void Start () {
        defaultMaterial = defaultMaterialInspector;
        hitMaterial = hitMaterialInspector;
        deadMaterial = deadMaterialInspector;

        isPaused = false;
    }
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown(KeyCode.Escape) && isPaused == false && PlayerController.player.IsDead == false) //toggles pause menu
        {
            CreatureEquipUI.isDisplayed = false;
            isPaused = true;
            pauseMenu.SetActive(true);
            Time.timeScale = 0;
            AudioListener.pause = true;
        }
        else if(Input.GetKeyDown(KeyCode.Escape) && isPaused == true)
        {
            isPaused = false;
            pauseMenu.SetActive(false);
            Time.timeScale = 1;
            AudioListener.pause = false;
        }


        if (PlayerController.player.IsDead) //deals with respawn events after players death
        {
            deathScreen.SetActive(true);
            if (deathInputTimer >= 1f)
            {
                if (Input.anyKeyDown)
                {
                    PlayerController.player = new Actor();
                    PlayerController.player.Constructor(PlayerController.playerSave); //loads saved actor class

                    PlayerController.storedCreatures = new List<Creature>(PlayerController.storedCreaturesSave); //loads old creatuers from before death
                    for (int i = 0; i < AbilityController.equippedAbilitiesSave.Length; i++)
                    {
                        if (AbilityController.equippedAbilitiesSave[i].IsEquipped)
                            AbilityController.equippedAbilities[i] = AbilityController.equippedAbilitiesSave[i];
                        else
                            AbilityController.equippedAbilities[i] = new AbilityController.AbilityEquipSlot();
                    }
                    SceneController.StartSceneLoad("Hub");
                }
            }
            else
                deathInputTimer += Time.deltaTime;
        }
        else
            deathInputTimer = 0;
	}
}
