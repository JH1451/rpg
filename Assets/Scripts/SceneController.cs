﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class SceneController : MonoBehaviour { //deals with scene transitions and effects

    [SerializeField]
    private float transition = 0.5f;
    private static float transitionTimer = 0f;
    private static bool inTransition = false, transitionOut = true;
    private static string sceneName = "";

    private bool showCredits = false;

    [SerializeField]
    private GameObject titlePanel, creditPanel;

    private Vector3 creditPanelPosition;

    [SerializeField]
    UnityEngine.UI.Image transitionImage;
    
	void Start () {

        //implement singleton pattern
        if (GameObject.Find("Scene Controller") != this.gameObject)
            Destroy(this.gameObject);
        DontDestroyOnLoad(this);

        if (creditPanel != null)
            creditPanelPosition = creditPanel.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

        transitionImage.enabled = inTransition;

        transitionImage.fillClockwise = transitionOut;

        if (creditPanel != null)
        {
            if (inTransition)
                    creditPanel.transform.Translate(Vector3.down * 50f);
            else if (showCredits)
            {
                if (creditPanel.transform.position.y + 20 < creditPanelPosition.y)
                    creditPanel.transform.Translate(Vector3.up * 20f);
                else
                    creditPanel.transform.position = creditPanelPosition;
            }
            else
                creditPanel.transform.position = creditPanelPosition + (Vector3.down * 1000);
        }
        
        if (inTransition) //if scene is transitioning
        {
            transitionTimer += Time.deltaTime;

            if (titlePanel != null)
                titlePanel.transform.Translate(-Vector3.right * Time.deltaTime * 3000);

            if (transitionTimer < transition) //radial wipe effect using filled sprite
            {
                if (transitionOut)
                    transitionImage.fillAmount = transitionTimer / transition;
                else
                    transitionImage.fillAmount = (transition - transitionTimer) / transition;
            }
            else
            {
                if (transitionOut)
                {
                    transitionImage.fillAmount = 1;
                    transitionTimer = 0;
                    transitionOut = false;

                    SceneManager.LoadScene(sceneName); //loads new scene
                    NavMesh.RemoveAllNavMeshData(); //resets old navmesh data

                    try
                    {
                        if (AbilityController.equippedAbilities.Length != 0) //resets equipped abilities
                        {
                            for (int i = 0; i < AbilityController.equippedAbilities.Length; i++)
                                AbilityController.equippedAbilities[i].Remove();
                        }
                    }
                    catch { }
                }
                else
                {
                    transitionImage.fillAmount = 0;
                    transitionTimer = 0;
                    transitionOut = true;
                    inTransition = false;
                }
            }

        }
        else
            transitionTimer = 0;
	}

    public void StartGame()
    {
        StartSceneLoad("Hub");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ShowCredits()
    {
        showCredits = true;
    }
    
    public static void StartSceneLoad(string name) //triggers a scene transition
    {
        if (!inTransition && (name == "Dungeon"))
        {
            GameController.currentMapEnemyLevel++;
            Debug.Log("New Map Level :::::: " + GameController.currentMapEnemyLevel + " ::::::");
        }


        inTransition = true;
        sceneName = name;
    }
}
