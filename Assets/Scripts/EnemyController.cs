﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//controlls various static actions for every enemy, such as dropping loot or deciding what abilities an enemy can and cant use
public class EnemyController : MonoBehaviour {

    public GameObject energyDropObjectInspector, creatureDropObjectInspector, smokeObjectInspector;
    public static GameObject energyDropObject, creatureDropObject, smokeObject;
    public List<GameObject> holoCreaturesInput = new List<GameObject>();
    public static List<GameObject> holoCreatures = new List<GameObject>();
    public enum EnemyType { nullType, walking1, walking2, flying1, flying2, flying3, boss1 };

    public static List<Actor> levelEnemies = new List<Actor>();

    Actor actor;

    private void Start()
    {
        energyDropObject = energyDropObjectInspector;
        creatureDropObject = creatureDropObjectInspector;
        holoCreatures = holoCreaturesInput;
        if (smokeObjectInspector != null)
            smokeObject = smokeObjectInspector;
    }

    public static void DropObject(Vector3 position) //drops and energy pickup
    {
        Instantiate(energyDropObject, position, Quaternion.identity);
    }

    public static void DropObject(Vector3 position, Creature storedCreature) //drops a creature pickup
    {
        switch (storedCreature.name)
        {
            case "Bug":
                creatureDropObject = holoCreatures[0];
                break;
            case "Dino":
                creatureDropObject = holoCreatures[1];
                break;
            case "Drone":
                creatureDropObject = holoCreatures[2];
                break;
            case "Wasp":
                creatureDropObject = holoCreatures[3];
                break;
            case "Arm":
                creatureDropObject = holoCreatures[4];
                break;
        }
        //if(storedCreature.name == "Bug")
        //{
        //    creatureDropObject = holoCreatures[0];
        //}
    
        //if(storedCreature.name == "Dino")
        //{
        //    creatureDropObject = holoCreatures[1];
        //}
        //if(storedCreature.name == "Drone")
        //{
        //    creatureDropObject = holoCreatures[2];
        //}
        //if(storedCreature.name == "Wasp")
        //{
        //    creatureDropObject = holoCreatures[3];
        //}
        //if(storedCreature.name == "Arm")
        //{
        //    creatureDropObject = holoCreatures[4];
        //}
        GameObject pickupObject = Instantiate(creatureDropObject, position, Quaternion.identity);
        pickupObject.GetComponent<Pickup>().storedCreature = storedCreature;
    }

    public static List<Ability.Type> GetAbilitiesForEnemyType(EnemyType type) //returns what abilities a certain enemy type can have
    {
        Debug.Log(type);
        List<Ability.Type> returnAbilities = new List<Ability.Type>();

        switch (type)
        {
            case EnemyType.walking1:
                returnAbilities.Add(Ability.Type.shockwave);
                returnAbilities.Add(Ability.Type.explosion);
                break;
            case EnemyType.walking2:
                returnAbilities.Add(Ability.Type.shockwave);
                returnAbilities.Add(Ability.Type.explosion);
                break;
            case EnemyType.flying1:
                returnAbilities.Add(Ability.Type.laser);
                returnAbilities.Add(Ability.Type.projectile);
                returnAbilities.Add(Ability.Type.missile);
                break;
            case EnemyType.flying2:
                returnAbilities.Add(Ability.Type.laser);
                returnAbilities.Add(Ability.Type.projectile);
                break;
            case EnemyType.flying3:
                returnAbilities.Add(Ability.Type.laser);
                returnAbilities.Add(Ability.Type.shockwave);
                returnAbilities.Add(Ability.Type.projectile);
                break;
            case EnemyType.boss1:
                returnAbilities.Add(Ability.Type.laser);
                returnAbilities.Add(Ability.Type.projectile);
                returnAbilities.Add(Ability.Type.missile);
                break;
            default:
                break;
        }

        Debug.Log(returnAbilities.Count);
        return returnAbilities;
    }

    public static string GetNameFromEnemyType(EnemyType type, bool isSuper) //returns name from enemy type
    {
        string returnName = "";

        switch (type)
        {
            case EnemyType.walking1:
                returnName = "Bug";
                break;
            case EnemyType.walking2:
                returnName = "Dino";
                break;
            case EnemyType.flying1:
                returnName = "Drone";
                break;
            case EnemyType.flying2:
                returnName = "Wasp";
                break;
            case EnemyType.flying3:
                returnName = "Arm";
                break;
            default:
                returnName = "NULL";
                break;
        }

        if (isSuper)
            returnName = "Super " + returnName;

        return returnName;
    }
}
