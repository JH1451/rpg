﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour //controlls enemy movement and attack patterns
{

    [SerializeField]
    public EnemyController.EnemyType type = EnemyController.EnemyType.nullType;

    // Use this for initialization
    [SerializeField]
    float alertDistance = 15f, attackDistance = 1.3f, attackDelay = 0.2f, defaultMovementSpeed, randomisedCooldownTime = 1, particleEmitTimer = 0;
    [SerializeField]
    Ability ability;
    Actor actor;
    NavMeshAgent agent;
    Creature creature;
    CharacterAnimator characterAnimator;

    [SerializeField]
    GameObject particleObject, smokeObject, mapSprite;

    const float startSmokingAtHealthPercent = 0.5f;

    public bool capturable = false, isChasing = false;
    public float stunTimer = 0, abilityAttackTimer = 0;
    public bool IsStunned { get { return stunTimer > 0; } }

    public bool CooldownExpired { get { return abilityAttackTimer >= randomisedCooldownTime; } }
    public float DistanceToPlayer { get { return Vector3.Distance(transform.position, PlayerController.playerObject.transform.position); } } 
    public bool isSmoking { get { return startSmokingAtHealthPercent >= ((float)actor.Health / (float)actor.MaxHealth); } }

    private float attackDelayCounter;

    void Start()
    {
        actor = GetComponent<Actor>();
        agent = GetComponent<NavMeshAgent>();
        defaultMovementSpeed = agent.speed;
        EnemyController.levelEnemies.Add(actor);
        creature = actor.enemyCreature;
        characterAnimator = GetComponent<CharacterAnimator>();

        particleObject.SetActive(true);
    }

    void Update()
    {
        float distance = Mathf.Abs(Vector3.Distance(this.transform.position, PlayerController.playerObject.transform.position));

        if (!actor.IsDead && isSmoking)
        {
            if (particleEmitTimer >= 0.2f)
            {
                particleEmitTimer = 0;

                ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams();
                emitParams.startLifetime = 2f;
                emitParams.position = Vector3.zero;

                particleObject.GetComponent<ParticleSystem>().Emit(emitParams, 1); //emits a smoke particle every 0.2 seconds
            }
            particleEmitTimer += Time.deltaTime;
        }
        else
        {
            particleObject.GetComponent<ParticleSystem>().Clear();
            particleObject.GetComponent<ParticleSystem>().Stop();
        }


        if (distance <= alertDistance)
        {
            mapSprite.SetActive(true);
            bool isLowHealth = (actor.Health <= (actor.MaxHealth / 10)), //is health at or lower than 10%
                 abilityInCooldown = (abilityAttackTimer < randomisedCooldownTime);

            if (isLowHealth) //retreat if low health
                Retreat();
            else
            {
                if (creature.active.EnemiesCanUse) //if enemy can use ability, retreat if player gets too close
                {
                    if (DistanceToPlayer < 6f)
                        Retreat();
                    else if (DistanceToPlayer > 14f)
                        Chase();
                }
                else
                    Chase(); //go towards player
            }

            if (creature.active.EnemiesCanUse) //does the enemy have an ability it can use against the player?
            {
                if (!IsStunned && !actor.IsDead)
                {
                    if (CooldownExpired)
                    {
                        agent.stoppingDistance = 6f;
                        abilityAttackTimer = 0;
                        randomisedCooldownTime = (creature.active.cooldown * 0.5f) + ((float)Random.Range(0, 200) / 1000); //randomises the cooldown time
                        AbilityController.UseEnemyAbility(creature.active.abilityType, transform.position, GetEnemyToPlayerRotation(), (int)(creature.active.damage * 0.6f)); //uses ability against player
                        if (tag == "FloatBot")
                            characterAnimator.OnAttack();
                    }
                    else
                    {
                        agent.stoppingDistance = 1f;
                        abilityAttackTimer += Time.deltaTime;
                    }
                }
            }
            else
            {
                agent.stoppingDistance = 1.5f;
                if (distance <= agent.stoppingDistance)
                    AttackPlayer();
                else
                    attackDelayCounter = 0;
            }
        }


        if (IsStunned)
        {
            agent.speed = defaultMovementSpeed * 0.2f; //stunned speed
            stunTimer -= Time.deltaTime;
        }
        else if (isChasing)
            agent.speed = defaultMovementSpeed; //normal speed
        else
            agent.speed = defaultMovementSpeed * 0.5f; //retreat speed

        if (actor.IsDead)
        {
            if (agent.isOnNavMesh)
                agent.isStopped = true;

            if(tag == "FloatBot")
            {
                StartCoroutine(Wait());
            }
            
            else if (!capturable)
            {
                EnemyController.DropObject(transform.position); //drops a pickup
                Destroy(this.gameObject);
            }
            else
            {
                EnemyController.DropObject(transform.position, actor.enemyCreature); //drops a pickup
                Destroy(this.gameObject);
            }
        }
    }

    void Chase() //go towards player
    {
        isChasing = true;

        Vector3 direction = PlayerController.playerObject.transform.position - transform.position;

        direction = direction.normalized * 5;

        if (agent.isOnNavMesh)
            agent.destination = transform.position + direction;
    }

    void Retreat() //go away from player
    {
        isChasing = false;

        Vector3 oppositeDirection = transform.position - PlayerController.playerObject.transform.position;

        oppositeDirection = oppositeDirection.normalized * 5;

        if (agent.isOnNavMesh)
            agent.destination = transform.position + oppositeDirection;

        Debug.DrawLine(transform.position, transform.position + oppositeDirection, Color.blue, 10f);
    }

    public void AttackPlayer()
    {
        if (attackDelayCounter >= attackDelay)
        {
            attackDelayCounter = 0;
            actor.Attack(PlayerController.player, actor.EnemyAttack);
            if (tag == "FloatBot")
            {
                characterAnimator.OnAttack(); //this currently causes an error on enemies without animation, so thats why its in an if statement
            }
        }
        else
            attackDelayCounter += Time.deltaTime;
    }

    public Quaternion GetEnemyToPlayerRotation()
    {
        Quaternion returnValue = Quaternion.identity;
        Vector3 enemyPos = new Vector3(transform.position.x, 0, transform.position.z);
        Vector3 playerPos = new Vector3(PlayerController.playerObject.transform.position.x, 0, PlayerController.playerObject.transform.position.z);
        Vector3 direction = playerPos - enemyPos; //get the direction between mouse and player
        float angle = Vector3.Angle(Vector3.forward, direction); //find the angle that the object needs to be rotated for that direction
        if (direction.x < 0) //if the direction is pointing to the left, reverse the direction
            angle = -angle;
        returnValue.eulerAngles = new Vector3(0, angle, 0); //create a rotation at the specified angle on the y = 0 plane
        return returnValue;
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3.3f);
        if (!capturable)
            {
                EnemyController.DropObject(transform.position); //drops a pickup
                Destroy(this.gameObject);
            }
            else
            {
                EnemyController.DropObject(transform.position, actor.enemyCreature); //drops a pickup
                Destroy(this.gameObject);
            }
    }
}