﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MapGenerator : MonoBehaviour { //sequentially generates a map from different tiles
    
    [SerializeField]
    [Tooltip("Distance between each block")]
    float distance = 1f;
    [SerializeField]
    [Tooltip("The amount of blocks in the main path")]
    int count = 5;
    [SerializeField]
    GameObject blockDefault, transitionVolume;
    [SerializeField]
    [Tooltip("Layer mask containing desired mesh colliders")]
    private LayerMask navMask, interactionMask;
    [SerializeField]
    List<Block> blocks = new List<Block>();
    [SerializeField]
    List<Block> deadEndBlocks = new List<Block>();
    

    private Vector3 currentPosition, nextPosition;
    private Direction currentDirection = Direction.forward, nextDirection;
    private List<GameObject> placedBlocks = new List<GameObject>();
    private bool hasEnded = false, generateNewMesh = false;

    public enum Direction { none, forward, back, left, right } //custom type for directions

    [System.Serializable]
    public class Block    //custom class for information about each block
    {
        public string name;
        public GameObject block;
        public List<Direction> ports;
    };
	
	void Start () {
        currentPosition = transform.position;

        this.gameObject.AddComponent<BoxCollider>();

        GenerateMap();
        FillInSpaces();
        RemoveBoxColliders();

        Destroy(this.gameObject.GetComponent<BoxCollider>());
        generateNewMesh = true;
    }

    private void LateUpdate()
    {
        if (generateNewMesh)
        {
            generateNewMesh = false;
            GenerateNavMesh();
        }
    }

    void GenerateMap()
	{
		for (int i = 0; i < count; i++)
		{
            if (hasEnded)
                break;

            currentPosition = GetPositionFromDirection(currentPosition, currentDirection); //get position to place next block

            List<Block> availableBlocks = new List<Block>();
            availableBlocks = GetAvailableBlocks(CheckDirections(currentPosition, currentDirection)); //get a list of suitable blocks

            int blockSelection = 0;
            if (i == (count - 1)) //if the path has ended, place an exit
            {
                blockSelection = 8;
                Instantiate(transitionVolume, currentPosition, Quaternion.identity);
                availableBlocks = blocks;
            }
            else
                blockSelection = UnityEngine.Random.Range(0, availableBlocks.Count); //if not make a random selection from suitable blocks
            

            PlaceBlock(availableBlocks[blockSelection].block, currentPosition, false, currentDirection); //place the block

            if (availableBlocks[blockSelection].ports.Count > 0) //if the block has a direction, choose randomly from them
                nextDirection = GetWorldDirection(currentDirection, availableBlocks[blockSelection].ports[UnityEngine.Random.Range(0, availableBlocks[blockSelection].ports.Count)]);

            if (availableBlocks[blockSelection].ports.Count > 1) //create a dead end at all the unused directions
            {
                for (int j = 0; j < availableBlocks[blockSelection].ports.Count; j++)
                {
                    if (GetWorldDirection(currentDirection, availableBlocks[blockSelection].ports[j]) != nextDirection)
                        GenerateDeadEnd(GetWorldDirection(currentDirection, availableBlocks[blockSelection].ports[j]));
                }
            }
            currentDirection = nextDirection;
        }
	}

    Vector3 GetPositionFromDirection(Vector3 position, Direction input) //returns the next position from a given direction
    {
        Vector3 newPosition = position;
        switch (input)
        {
            case Direction.forward:
                newPosition += (Vector3.forward * distance);
                break;
            case Direction.back:
                newPosition += (Vector3.back * distance);
                break;
            case Direction.left:
                newPosition += (Vector3.left * distance);
                break;
            case Direction.right:
                newPosition += (Vector3.right * distance);
                break;
        }

        return newPosition;
    }

    Quaternion GetRotationFromDirection(Direction input) //returns a direction as a quaternion
    {
        Quaternion rotation = Quaternion.identity;

        switch (input)
        {
            case Direction.forward:
                rotation =  Quaternion.Euler(0f, 0f, 0f);
                break;
            case Direction.back:
                rotation = Quaternion.Euler(0f, 180f, 0f);
                break;
            case Direction.left:
                rotation = Quaternion.Euler(0f, 270f, 0f);
                break;
            case Direction.right:
                rotation = Quaternion.Euler(0f, 90f, 0f);
                break;
        }
        return rotation;
    }

    void PlaceBlock(GameObject block, Vector3 position, bool isDeadEnd, Direction input) //places a block
    {
        placedBlocks.Add(Instantiate(block, position, GetRotationFromDirection(input), this.transform));
        placedBlocks[placedBlocks.Count - 1].AddComponent<BoxCollider>();
    }

    public void GenerateDeadEnd(Direction input) //creates a dead end, can be expanded upon
    {
        //List<Block> deadEnds = new List<Block>();
        //deadEnds.Add(blocks[7]);
        //deadEnds.Add(blocks[9]);

        Block deadEndToPlace = deadEndBlocks[UnityEngine.Random.Range(0, deadEndBlocks.Count)];

        PlaceBlock(deadEndToPlace.block, GetPositionFromDirection(currentPosition, input), true, input);
        //PlaceBlock(blocks[7].block, GetPositionFromDirection(currentPosition, input), true, input);
        //surfaces.Add();
    }

    public bool CheckDirection(Direction input) //checks for collisions in a given direction
    {
        Vector3 position = currentPosition;
        switch (input)
        {
            case Direction.none:
                break;
            case Direction.forward:
                position += Vector3.forward * distance;
                break;
            case Direction.back:
                position += Vector3.back * distance;
                break;
            case Direction.left:
                position += Vector3.left * distance;
                break;
            case Direction.right:
                position += Vector3.right * distance;
                break;
            default:
                break;
        }

        if (Physics.CheckSphere(position, 0.01f))
            return false;
        else
            return true;
    }

    public bool CheckDirection(Direction input1, Direction input2) //overload of CheckDirection(), checks two directions
    {
        if (CheckDirection(input1) && CheckDirection(input2))
            return true;
        else
            return false;
    }

    public bool CheckDirection(Direction input1, Direction input2, Direction input3) //overload of CheckDirection(), checks three directions
    {
        if (CheckDirection(input1) && CheckDirection(input2) && CheckDirection(input3))
            return true;
        else
            return false;
    }

    Direction GetWorldDirection(Direction current, Direction target) //get world direction from local direction
    {
        int rotation = 0, translation = 0;

        switch (current)
        {
            case Direction.forward:
                rotation = 0;
                break;
            case Direction.back:
                rotation = 180;
                break;
            case Direction.left:
                rotation = 270;
                break;
            case Direction.right:
                rotation = 90;
                break;
            default:
                break;
        }

        switch (target)
        {
            case Direction.forward:
                translation = 0;
                break;
            case Direction.back:
                translation = 180;
                break;
            case Direction.left:
                translation = 270;
                break;
            case Direction.right:
                translation = 90;
                break;
            default:
                break;
        }

        rotation += translation;

        if (rotation >= 360)
            rotation -= 360;

        if (rotation < 0)
            rotation += 360;

        switch (rotation)
        {
            case 0:
                return Direction.forward;
            case 180:
                return Direction.back;
            case 270:
                return Direction.left;
            case 90:
                return Direction.right;
            default:
                Debug.LogError("Rotation not matched");
                return Direction.forward;
        }
    }

    List<Direction> CheckDirections(Vector3 position, Direction direction) //check 3 possible directions for next block
    {
        List<Direction> returnDirections = new List<Direction>();

        switch (currentDirection)
        {
            case Direction.forward:
                if (CheckDirection(Direction.left))
                    returnDirections.Add(Direction.left);
                if (CheckDirection(Direction.forward))
                    returnDirections.Add(Direction.forward);
                if (CheckDirection(Direction.right))
                    returnDirections.Add(Direction.right);
                break;
            case Direction.back:
                if (CheckDirection(Direction.left))
                    returnDirections.Add(Direction.left);
                if (CheckDirection(Direction.back))
                    returnDirections.Add(Direction.back);
                if (CheckDirection(Direction.right))
                    returnDirections.Add(Direction.right);
                break;
            case Direction.left:
                if (CheckDirection(Direction.left))
                    returnDirections.Add(Direction.left);
                if (CheckDirection(Direction.forward))
                    returnDirections.Add(Direction.forward);
                if (CheckDirection(Direction.back))
                    returnDirections.Add(Direction.back);
                break;
            case Direction.right:
                if (CheckDirection(Direction.right))
                    returnDirections.Add(Direction.right);
                if (CheckDirection(Direction.forward))
                    returnDirections.Add(Direction.forward);
                if (CheckDirection(Direction.back))
                    returnDirections.Add(Direction.back);
                break;
            default:
                break;
        }
        
        return returnDirections;
    }

    List<Block> GetAvailableBlocks(List<Direction> input) //gets the suitable blocks that fit the available spaces
    {
        List<Block> returnBlocks = new List<Block>();

        for (int i = 0; i < blocks.Count; i++)
        {
            bool containsAllPorts = false;
            
            for (int j = 0; j < blocks[i].ports.Count; j++) //scan all of the ports on all of the blocks to see if it matches the input
            {
                if (!input.Contains(GetWorldDirection(currentDirection, blocks[i].ports[j])))
                {
                    containsAllPorts = false;
                    break;
                }
                else
                    containsAllPorts = true;
            }

            if (containsAllPorts) //if the ports match, add the block to the return list
                returnBlocks.Add(blocks[i]);
        }

        if (returnBlocks.Count == 0) //if there is no available spaces, place a normal room
        {
            hasEnded = true;
            returnBlocks.Add(blocks[8]);
        }

        return returnBlocks;
    }

    void GenerateNavMesh()
    {
        Bounds boundsMesh = new Bounds(Vector3.zero + new Vector3(0, -3f, 0), new Vector3(1000, 1, 1000));
        NavMeshData navMeshData;
        NavMesh.RemoveAllNavMeshData();

        NavMeshBuildSettings settings = NavMesh.GetSettingsByID(0);
        settings.minRegionArea = 1000f;
        settings.agentHeight = 5f;
        settings.overrideVoxelSize = true;
        settings.voxelSize = 0.2f;

        List<NavMeshBuildSource> surfaces = new List<NavMeshBuildSource>();
        NavMeshBuilder.CollectSources(boundsMesh, navMask, NavMeshCollectGeometry.PhysicsColliders, 0, new List<NavMeshBuildMarkup>(), surfaces);

        navMeshData = NavMeshBuilder.BuildNavMeshData(settings, surfaces, boundsMesh, transform.position, transform.rotation);

        NavMesh.AddNavMeshData(navMeshData);
    }

    void RemoveBoxColliders()
    {
        for (int i = 0; i < placedBlocks.Count; i++)
        {
            Destroy(placedBlocks[i].GetComponent<BoxCollider>());
        }
    }

    bool BlockPlacedAtPosition(Vector3 position)
    {
        if (Vector3.Distance(position, Vector3.zero) <= 0.1f)
            return true;

        for (int i = 0; i < placedBlocks.Count; i++)
        {
            if (Vector3.Distance(position, placedBlocks[i].transform.position) <= 0.1)
                return true;
        }
        return false;
    }

    void FillInSpaces()
    {
        float bounds = 11 * distance; //max distance in each direction that should be filled in

        for (float i = -bounds; i < bounds; i += distance)
        {
            for (float j = -bounds; j < bounds; j += distance)
            {
                Vector3 currentPosition = new Vector3(i, 0, j);
                if (!BlockPlacedAtPosition(currentPosition))
                {
                    Instantiate(blockDefault, currentPosition, Quaternion.identity).transform.parent = this.transform;
                }
            }
        }
    }
}